### 更新说明

最新Guns 7.0已全面升级，整体模块化重构，功能更加丰富，细化。

### 配套手册

[https://doc.stylefeng.cn/](https://doc.stylefeng.cn/)

### 快速启动

1. 在mysql数据库中创建guns数据库，推荐mysql 5.7版本。
   
2. 修改`application-local.yml`中的数据库连接配置连接到您的数据库。

3. 打开`GunsApplication`运行main方法即可启动，注：新版无需运行sql初始化文件，因为集成了flyway会自动初始化表。
