package cn.stylefeng.guns.modular.business.controller;

import cn.stylefeng.guns.modular.business.entity.Sample;
import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.SuccessResponseData;
import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.PostResource;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 文件管理界面
 *
 * @author lgq
 * @date 2021/1/9
 */
@Controller
@ApiResource(name = "典型案例界面")
public class ClassicCaseController{
	/**
	 * lucky病毒
	 * @return
	 */
	@GetResource(name = "lucky病毒", path = "/view/classicCase/lucky")
	public String lucky() {
		return "modular/business/classicCase/lucky.html";
	}
	
	@GetResource(name = "XiaoBa病毒", path = "/view/classicCase/xiaoba")
	public String XiaoBa() {
		return "modular/business/classicCase/xiaoba.html";
	}
	@GetResource(name = "satan病毒", path = "/view/classicCase/satan")
	public String satan() {
		return "modular/business/classicCase/satan.html";
	}
	@GetResource(name = "xdata病毒", path = "/view/classicCase/xdata")
	public String xdata() {
		return "modular/business/classicCase/xdata.html";
	}
	@GetResource(name = "wannadie病毒", path = "/view/classicCase/wannadie")
	public String wannadie() {
		return "modular/business/classicCase/wannadie.html";
	}
	@GetResource(name = "wannaren病毒", path = "/view/classicCase/wannaren")
	public String wannaren() {
		return "modular/business/classicCase/wannaren.html";
	}
	@PostResource(name = "打开虚拟机样本", path = "/classicCase/openVM")
	public ResponseData openVM(String bingDu) throws IOException {
		openVMWithName(bingDu);
		return new SuccessResponseData(bingDu);
	}
	
	
	public void openVMWithName(String bingDu) {
		String  path="C:/CuckooFile/classicCase/";
		path = path + bingDu +".bat";
		try {

			Runtime.getRuntime().exec("cmd.exe /c "+ path);
			//Runtime.getRuntime().exec("rundll32 url.dll FileProtocolHandler " + path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
