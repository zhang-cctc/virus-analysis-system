package cn.stylefeng.guns.modular.business.controller;

import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Decription: 首页及各模块简介页
 * @author wangtao
 * @version 1.0
 * create on 2021/6/2.
 *
 */
@Controller
@ApiResource(name = "首页及介绍页")
@RequestMapping("/view/home")
public class HomeController {

	@GetResource(name = "首页", path = "index")
	public String  index() {
		return "modular/home/home.html";
	}

	@GetResource(name = "勒索病毒专栏", path = "virus")
	public String virus() {
		return "modular/home/virus.html";
	}

	@GetResource(name = "样本管理", path = "sample")
	public String sample() {
		return "modular/home/sample.html";
	}

	@GetResource(name = "密码算法识别", path = "rules")
	public String rules() {
		return "modular/home/rules.html";
	}

	@GetResource(name = "样本分析cuckoo", path = "cuckoo")
	public String cuckoo() {
		return "modular/home/cuckoo.html";
	}

	@GetResource(name = "样本分析virustotal", path = "virustotal")
	public String virustotal() {
		return "modular/home/virustotal.html";
	}

	@GetResource(name = "工具集", path = "packing")
	public String packing() {
		return "modular/home/packing.html";
	}

	/**
	 * 典型案例
	 * @return
	 */
	@GetResource(name = "典型案例", path = "classicCase")
	public String classicCase() {
		return "modular/home/classicCase.html";
	}

}
