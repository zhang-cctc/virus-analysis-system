package cn.stylefeng.guns.modular.business.controller;

import cn.hutool.core.io.FileUtil;
import cn.stylefeng.guns.modular.business.entity.Sample;
import cn.stylefeng.roses.kernel.file.api.util.DownloadUtil;
import cn.stylefeng.roses.kernel.rule.pojo.response.ErrorResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.SuccessResponseData;
import cn.stylefeng.roses.kernel.rule.util.HttpServletUtil;
import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.PostResource;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 文件管理界面
 *
 * @author lgq
 * @date 2021/1/9
 */
@Controller
@ApiResource(name = "加壳分析界面")
public class PackingController {

    
    /**  
     * <p> 
         *   跳转加壳分析首页
     * </p>
     * 	@author 	    lbw
     * 	@date 			2021年5月10日 下午6:00:01
     * 	@version V3.1  
     *	@return	
     */ 
    @GetResource(name = "加壳分析首页", path = "/view/packing")
    public String samalIndex() {
    	return "modular/business/packing/packingindex.html";
    }
   
    @GetResource(name = "加壳分析详情", path = "/packing/packingShow")
    public String packingIndex() {
    	System.out.println("进入加壳分析详情");
    	return "modular/business/packing/packingShow.html";
    }
    
    @GetResource(name = "查壳工具", path = "/view/packing/chake")
    public String chake() {
    	return "modular/business/packing/chake.html";
    }
    @GetResource(name = "脱壳工具", path = "/view/packing/tuoke")
    public String tuoke() {
    	return "modular/business/packing/tuoke.html";
    }
    @GetResource(name = "反汇编工具", path = "/view/packing/fanhuibian")
    public String fanhuibian() {
    	return "modular/business/packing/fanhuibian.html";
    }
    @GetResource(name = "动态调试工具", path = "/view/packing/dongtaitiaoshi")
    public String dongtaitiaoshi() {
    	return "modular/business/packing/dongtaitiaoshi.html";
    }
    @GetResource(name = "进程监测工具", path = "/view/packing/jinchengjiance")
    public String jinchengjiance() {
    	return "modular/business/packing/jinchengjiance.html";
    }
    @GetResource(name = "注册表监测工具", path = "/view/packing/zhucebiaojiance")
    public String zhucebiaojiance() {
    	return "modular/business/packing/zhucebiaojiance.html";
    }
    @GetResource(name = "网络监测工具", path = "/view/packing/wangluojiance")
    public String wangluojiance() {
    	return "modular/business/packing/wangluojiance.html";
    }
    
    
    /**
	 * 打开应用
	 *
	 * @author 
	 * @date 2020/11/29 11:19
	 */
	@PostResource(name = "打开应用", path = "/packing/open", requiredPermission = false)
	public ResponseData open(@RequestParam String openPath) {
			//System.out.println(params.get("path"));
		
			try {
				Runtime.getRuntime().exec("rundll32 url.dll FileProtocolHandler " + openPath);
			} catch (IOException e) {
				e.printStackTrace();
				return new ErrorResponseData("PackingController.java","无法打开文件");
			}
		
		return new SuccessResponseData();
	}
	/**
	 * 下载应用
	 *
	 * @author 
	 * @date 2020/11/29 11:19
	 */
	@GetResource(name = "下载应用", path = "/packing/download", requiredPermission = false)
	public ResponseData download(@RequestParam String downloadPath) {
		File file = new File(downloadPath);
		
		HttpServletResponse response = HttpServletUtil.getResponse();
		if (file.exists() && file.isFile()) {
			byte[] bytes = FileUtil.readBytes(file.getAbsolutePath());			
			DownloadUtil.download(file.getName(), bytes, response);
		} else {
			response.setStatus(404);
			return new ErrorResponseData("PackingController.java","无法下载文件");
		}
		return new SuccessResponseData();
	}
    
    
}
