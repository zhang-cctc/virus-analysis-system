package cn.stylefeng.guns.modular.business.controller;

import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import org.springframework.stereotype.Controller;

/**
 * <p> 
 *   规则管理页面跳转
 * </p>
 *
 *
 * @author lbw 
 * 2021年5月13日
 *
 */
@Controller
@ApiResource(name = "规则管理界面")
public class RuleViewController {

    @GetResource(name = "规则页面", path = "/view/rules")
    public String viewRules() {
    	return "modular/business/rules/rules.html";
    }
    
    @GetResource(name = "添加规则页面", path = "/view/rule/add")
    public String viewAddRule() {
    	return "modular/business/rules/rule_add.html";
    }
    
    @GetResource(name = "编辑规则页面", path = "/view/rule/edit")
    public String viewEditRule() {
    	return "modular/business/rules/rule_edit.html";
    }
    
    @GetResource(name = "案例页面", path = "/view/case")
    public String viewCase() {
    	return "modular/business/rules/case.html";
    }

}
