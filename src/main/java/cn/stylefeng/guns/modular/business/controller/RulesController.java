package cn.stylefeng.guns.modular.business.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.stylefeng.guns.modular.business.entity.Rule;
import cn.stylefeng.guns.modular.business.service.RuleService;
import cn.stylefeng.roses.kernel.db.api.pojo.page.PageResult;
import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.SuccessResponseData;
import cn.stylefeng.roses.kernel.rule.util.HttpServletUtil;
import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.PostResource;

@RestController
@ApiResource(name = "规则管理相关接口")
public class RulesController {
	
	@Resource
	private RuleService ruleService;

	@GetResource(name = "分页规则列表", path = "/rule/rulelistpage", requiredPermission = false)
	public ResponseData fileInfoListPage(Rule rule) {
		PageResult<Rule> ruleListPage = ruleService.ruleListPage(rule);
		return new SuccessResponseData(ruleListPage);
	}

	@PostResource(name = "添加规则", path = "/rule/addrule", requiredPermission = false)
	public ResponseData addRule(Rule rule) {
		ruleService.save(rule);
		return new SuccessResponseData();
	}

	@PostResource(name = "规则删除", path = "/rule/deleterule")
	public ResponseData delete(@RequestBody @Validated(Rule.class) Rule rule) {
		ruleService.removeById(rule.getRuleId());
		return new SuccessResponseData();
	}

	@PostResource(name = "规则编辑", path = "/rule/editrule")
	public ResponseData edit(@RequestBody @Validated(Rule.class) Rule rule) {
		ruleService.saveOrUpdate(rule);
		return new SuccessResponseData();
	}

	@GetResource(name = "规则查看", path = "/rule/detailrule")
	public ResponseData detail(@Validated(Rule.class) Rule rule) {
		return new SuccessResponseData(ruleService.getById(rule.getRuleId()));
	}

	
	@GetResource(name = "导出规则", path = "/rule/exportrules", requiredLogin = false, requiredPermission = false) 
	public void exportRules() {
	  HttpServletResponse response = HttpServletUtil.getResponse(); 
	  ruleService.exportRules(response);
	}
	 
	@GetResource(name = "下载文件", path = "/rule/downloadfilees", requiredLogin = false, requiredPermission = false) 
	public void downloadFiles(@RequestParam("filename") String filename) {
		HttpServletResponse response = HttpServletUtil.getResponse(); 
		ruleService.downloadRules(response,filename);
	}
	
}
