/*
 * Copyright [2020-2030] [https://www.stylefeng.cn]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Guns采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Guns源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://gitee.com/stylefeng/guns
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/stylefeng/guns
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */
package cn.stylefeng.guns.modular.business.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import cn.stylefeng.guns.modular.business.entity.Sample;
import cn.stylefeng.guns.modular.business.entity.SampleType;
import cn.stylefeng.guns.modular.business.service.SampleService;
import cn.stylefeng.guns.modular.business.utils.ScctcConst;
import cn.stylefeng.roses.kernel.rule.pojo.response.ErrorResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.SuccessResponseData;
import cn.stylefeng.roses.kernel.rule.util.HttpServletUtil;
import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.PostResource;

/**
 * @author majianguo
 * @date 2020/12/27 13:39
 */
@RestController
@ApiResource(name = "样本管理相关接口")
public class SampleController {

	@Resource
	private SampleService sampleService;

	/**
	 * 上传文件
	 *
	 * @author majianguo
	 * @date 2020/12/27 13:17
	 */
	/*
	 * @PostResource(name = "上传文件", path = "/Sample/upload", requiredPermission =
	 * false) public ResponseData upload(@RequestPart("file") MultipartFile
	 * file, @Validated(Sample.class) Sample sample) { Sample fileUploadInfoResult =
	 * this.sampleService.uploadFile(file); return new
	 * SuccessResponseData(fileUploadInfoResult); }
	 */

	/**
	 * 根据附件IDS查询附件信息
	 *
	 * @param fileIds 附件IDS
	 * @return 附件返回类
	 * @author majianguo
	 * @date 2020/12/27 13:17
	 */
	@GetResource(name = "根据附件IDS查询附件信息", path = "/Sample/getSampleListByFileIds", requiredPermission = false)
	public ResponseData getSampleListByFileIds(@RequestParam(value = "sampleIds") String sampleIds) {
		List<Sample> list = this.sampleService.getSampleListByFileIds(sampleIds);
		return new SuccessResponseData(list);
	}

	/**
	 * 删除文件信息（真删除文件信息）
	 *
	 * @author fengshuonan
	 * @date 2020/11/29 11:19
	 */
	@PostResource(name = "删除文件信息（真删除文件信息）", path = "/Sample/deleteReally", requiredPermission = false)
	public ResponseData deleteReally(@RequestBody @Validated(Sample.class) Sample sample) {
		this.sampleService.deleteReally(sample);
		return new SuccessResponseData();
	}

	/**
	 * 分页查询文件信息表
	 *
	 * @author fengshuonan
	 * @date 2020/11/29 11:29
	 */
	@GetResource(name = "样本对象表", path = "/Sample/sampleListPage", requiredPermission = false)
	public ResponseData sampleListPage(Sample sample) {
		return new SuccessResponseData(this.sampleService.sampleListPage(sample));
	}
	/**
	 * 分页查询文件信息表
	 *
	 * @author fengshuonan
	 * @date 2020/11/29 11:29
	 */
	@GetResource(name = "未分析样本对象表", path = "/Sample/sampleListPageByStatus0", requiredPermission = false)
	public ResponseData sampleListPageByStatus0(Sample sample) {
		return new SuccessResponseData(this.sampleService.sampleListPageByStatus0(sample));
	}
	
	/**
	 * 分页查询文件信息表
	 *
	 * @author fengshuonan
	 * @date 2020/11/29 11:29
	 */
	@GetResource(name = "已分析样本对象表", path = "/Sample/sampleListPageByStatus1", requiredPermission = false)
	public ResponseData sampleListPageByStatus1(Sample sample) {
		return new SuccessResponseData(this.sampleService.sampleListPageByStatus1(sample));
	}
	/*    *//**
			 * 确认替换附件
			 * <p>
			 * 在替换接口替换文件以后，需要调用本接口替换操作才会生效
			 *
			 * @author majianguo
			 * @date 2020/12/27 13:18
			 *//*
				 * @PostResource(name = "确认替换附件", path = "/Sample/confirmReplaceFile",
				 * requiredPermission = false) public ResponseData
				 * confirmReplaceFile(@RequestBody List<Long> fileIdList) {
				 * this.sysFileInfoService.confirmReplaceFile(fileIdList); return new
				 * SuccessResponseData(); }
				 */

	/**
	 * 查看详情文件信息表
	 *
	 * @author fengshuonan
	 * @date 2020/11/29 11:29
	 */
	@GetResource(name = "查看详情文件信息表", path = "/Sample/detail", requiredPermission = false)
	public ResponseData detail(@RequestParam(value = "sampleId") String sampleId) {
		long id = Long.valueOf(sampleId).longValue();
		return new SuccessResponseData(sampleService.detail(id));
	}
	/**
	 * 更新文件
	 *
	 * @author majianguo
	 * @date 2020/12/27 13:17
	 */
	@PostResource(name = "更新样本", path = "/Sample/update", requiredPermission = false)
	public ResponseData update(Sample sample) {
		Sample fileUploadInfoResult = this.sampleService.update(null,sample);
		return new SuccessResponseData(fileUploadInfoResult);
	}
	
	/**  
	 * <p> 
	 *   添加样本（包含上传文件）
	 * </p>
	 * 	@author 	    lbw
	 * 	@date 			2021年5月11日 上午11:12:37
	 *	@param file
	 *	@param sample
	 *	@return	
	 * @throws Exception 
	 */ 
	@PostResource(name = "上传文件", path = "/Sample/addSample", requiredPermission = false)
	public ResponseData addSample(@RequestPart("file") MultipartFile file, @Validated(Sample.class) Sample sample) throws Exception {
		if(file.isEmpty()) {
			return new ErrorResponseData("404","请选择样本");
		}
		Sample fileUploadInfoResult = this.sampleService.addSampleAndFile(file,sample);
		return new SuccessResponseData(fileUploadInfoResult);
	}
	/**  
	 * <p> 
	 *  批量上传文件，创建Sample
	 * </p>
	 * 	@author 	    lbw
	 * 	@date 			2021年5月11日 上午11:12:37
	 *	@param file
	 *	@param sample
	 *	@return	
	 */ 
	@PostResource(name = "批量上传文件", path = "/Sample/addSampleByFiles", requiredPermission = false)
	public ResponseData addSampleByFiles(@RequestPart("file") MultipartFile file) throws IOException {
		if(file.isEmpty()) {
			return new ErrorResponseData("404","请选择样本");
		}
		
		return new SuccessResponseData(this.sampleService.addSampleByFile(file));
	}
	/**  
	 * <p> 
	 *  分析样本
	 * </p>
	 * 	@author 	    lbw
	 * 	@date 			2021年5月11日 上午11:12:37
	 *	@param file
	 *	@param sample
	 *	@return	
	 */ 
	@PostResource(name = "分析样本", path = "/Sample/analysisSample")
	public ResponseData analysisSample(@RequestBody @Validated(Sample.class) Sample sample) throws IOException {
		
		return new SuccessResponseData(this.sampleService.analysisSample(sample));
	}
	
	
	@GetResource(name = "查看报告", path = "/Sample/viewreport", requiredPermission = false)
	public ResponseData viewReport(@RequestParam(value = "sampleId") String sampleId) {
		Sample detail = sampleService.detail(Long.valueOf(sampleId).longValue());
		Long taskId = detail.getTaskId();
		String viewReport = this.sampleService.viewReport(taskId);
		if(StringUtils.isEmpty(viewReport)) {
			return new ErrorResponseData("404", "Cuckoo api 服务不可用，修复后重试。");
		}else {
			return new SuccessResponseData(viewReport);
		}
	}
	
	@GetResource(name = "下载报告", path = "/Sample/downloadReport", requiredLogin = false, requiredPermission = false)
    public void downloadReport(@RequestParam(value = "sampleId") String sampleId) {
        HttpServletResponse response = HttpServletUtil.getResponse();
        Sample detail = sampleService.detail(Long.valueOf(sampleId).longValue());
		Long taskId = detail.getTaskId();
        this.sampleService.downloadReport(taskId, response);
    }
	
	@GetResource(name = "获取cuckoourl", path = "/Sample/getCuckooUrl", requiredPermission = false)
    public ResponseData getCuckooUrl() {
		return new SuccessResponseData(ScctcConst.CUCKOO_URL);
    }
	
	@GetResource(name = "获取cuckoo api 是否可用", path = "/Sample/testCuckooApi", requiredPermission = false)
	public ResponseData testCuckooApi() {
		String testCuckoo = sampleService.testCuckoo();
		if(StringUtils.isEmpty(testCuckoo)) {
			return new SuccessResponseData();
		}else {
			return new ErrorResponseData("404", testCuckoo);
		}
	}
	
	@GetResource(name = "统计样本类型占比", path = "/Sample/staticTypePer", requiredPermission = false)
	public ResponseData staticTypePer() {
		JSONArray staticTypePer = sampleService.staticTypePer();
		return new SuccessResponseData(staticTypePer);
	}
	@GetResource(name = "统计样本类型排行", path = "/Sample/staticTypeTop", requiredPermission = false)
	public ResponseData staticTypeTop() {
		JSONObject staticTypeTop = sampleService.staticTypeTop();
		return new SuccessResponseData(staticTypeTop);
	}
	@GetResource(name = "统计样本每日新增", path = "/Sample/staticFileCount", requiredPermission = false)
	public ResponseData staticFileCount() {
		JSONObject staticTypeTop = sampleService.staticFileCount();
		return new SuccessResponseData(staticTypeTop);
	}
	
}
