package cn.stylefeng.guns.modular.business.controller;

import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import org.springframework.stereotype.Controller;

/**
 * 文件管理界面
 *
 * @author lgq
 * @date 2021/1/9
 */
@Controller
@ApiResource(name = "文件管理界面")
public class SampleViewController {

    /**
     * 病毒样本管理首页
     *
     * @author lgq
     * @date 2021/1/9
     */
    @GetResource(name = "病毒样本管理首页", path = "/view/sample")
    public String fileIndex() {
        return "modular/business/sample/sample.html";
    }
    
    /**
     * 病毒样本分析
     *
     * @author lgq
     * @date 2021/1/9
     */
    @GetResource(name = "病毒样本分析首页", path = "/view/analysis")
    public String analysisIndex() {
        return "modular/business/analysis/analysis.html";
    }
    
    /**
     * 文件详情页面
     *
     * @author lgq
     * @date 2021/1/9
     */
    @GetResource(name = "文件详情页面", path = "/view/sampleDetails")
    public String details() {
        return "modular/business/sample/sample_details.html";
    }
    
    /**  
     * <p> 
     *   样本添加跳转
     * </p>
     * 	@author 	    lbw
     * 	@date 			2021年5月11日 下午3:17:51
     * 	@version V3.1  
     *	@return	
     */ 
    @GetResource(name = "样本添加", path = "/view/sample/add")
    public String sampleAdd() {
        return "modular/business/sample/sample_add.html";
    }
    
    
    @GetResource(name = "查看报告页面", path = "/view/analysis/viewReport")
    public String viewReport() {
        return "modular/business/analysis/viewReport.html";
    }
    @GetResource(name = "查看日志页面", path = "/view/analysis/viewLog")
    public String viewLog() {
        return "modular/business/analysis/viewLog.html";
    }
    @GetResource(name = "分析提取页面", path = "/view/analysis/getView")
    public String getView() {
        return "modular/business/analysis/getView.html";
    }
    @GetResource(name = "分析提取详情", path = "/view/analysis/getDetail", requiredPermission = false)
    public String getDetail() {
        return "modular/business/analysis/getDetail.html";
    }
    /**
     * 病毒样本统计
     * @return
     */
    @GetResource(name = "病毒样本统计", path = "/view/sample/static")
    public String sampleStatic() {
        return "modular/business/sample/sample_static.html";
    }
    
    /**
     * 病毒样本统计饼状图
     * @return
     */
    @GetResource(name = "病毒样本统计饼状图", path = "/view/sampleStatic/pie")
    public String staticPie() {
        return "modular/business/sampleStatic/pie.html";
    }
    /**
     * 病毒样本统计树状图
     * @return
     */
    @GetResource(name = "病毒样本统计树状图", path = "/view/sampleStatic/bar")
    public String staticBar() {
        return "modular/business/sampleStatic/bar.html";
    }
    
    /**
     * 病毒样本统计折线图
     * @return
     */
    @GetResource(name = "病毒样本统计树状图", path = "/view/sampleStatic/line")
    public String staticLine() {
        return "modular/business/sampleStatic/line.html";
    }
}
