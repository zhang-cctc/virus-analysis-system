package cn.stylefeng.guns.modular.business.controller;

import java.util.Date;

import javax.annotation.Resource;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import cn.stylefeng.guns.modular.business.entity.SampleType;
import cn.stylefeng.guns.modular.business.service.TypeService;
import cn.stylefeng.roses.kernel.db.api.pojo.page.PageResult;
import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.SuccessResponseData;
import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.PostResource;

@RestController
@ApiResource(name = "规则管理相关接口")
public class TypeController {
	
	@Resource
	private TypeService typeService;

	@GetResource(name = "分页规则列表", path = "/type/listpage", requiredPermission = false)
	public ResponseData fileInfoListPage(SampleType type) {
		PageResult<SampleType> typeListPage = typeService.typeListPage(type);
		return new SuccessResponseData(typeListPage);
	}

	@PostResource(name = "添加规则", path = "/type/add", requiredPermission = false)
	public ResponseData addType(SampleType type) {
		
		typeService.save(type);
		return new SuccessResponseData();
	}

	@PostResource(name = "规则删除", path = "/type/delete")
	public ResponseData delete(@RequestBody @Validated(SampleType.class) SampleType type) {
		typeService.removeById(type.getTypeId());
		return new SuccessResponseData();
	}

	@PostResource(name = "规则编辑", path = "/type/edit")
	public ResponseData edit(@RequestBody @Validated(SampleType.class) SampleType type) {
		typeService.saveOrUpdate(type);
		return new SuccessResponseData();
	}

	@GetResource(name = "规则查看", path = "/type/detail")
	public ResponseData detail(String typeId) {
		return new SuccessResponseData(typeService.getById(typeId));
	}

	
}
