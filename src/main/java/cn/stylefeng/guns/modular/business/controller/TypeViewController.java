package cn.stylefeng.guns.modular.business.controller;

import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import org.springframework.stereotype.Controller;

/**
 * <p> 
 *   规则管理页面跳转
 * </p>
 *
 *
 * @author lbw 
 * 2021年5月13日
 *
 */
@Controller
@ApiResource(name = "类别管理界面")
public class TypeViewController {

    @GetResource(name = "规则页面", path = "/view/sampleNew/type")
    public String viewRules() {
    	return "modular/business/sampleNew/type/type.html";
    }
    
    @GetResource(name = "添加规则页面", path = "/view/sampleNew/type/add")
    public String viewAddRule() {
    	return "modular/business/sampleNew/type/type_add.html";
    }
    
    @GetResource(name = "编辑规则页面", path = "/view/sampleNew/type/edit")
    public String viewEditRule() {
    	return "modular/business/sampleNew/type/type_edit.html";
    }
}
