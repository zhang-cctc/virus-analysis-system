package cn.stylefeng.guns.modular.business.controller;

import cn.hutool.core.io.FileUtil;
import cn.stylefeng.guns.modular.business.service.VirusService;
import cn.stylefeng.roses.kernel.file.api.util.DownloadUtil;
import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.SuccessResponseData;
import cn.stylefeng.roses.kernel.rule.util.HttpServletUtil;
import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.PostResource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONArray;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Decription: 
 * @author wangtao
 * @version 1.0
 * create on 2021/6/1.
 *
 */
@Controller
@ApiResource(name = "勒索病毒专栏管理")
@RequestMapping("/view/virus/")
public class VirusController {




	@Autowired
	private VirusService service;

	/**
	 * 加载新闻列表页面
	 * @return
	 */

	@GetResource(name = "新闻列表", path = "news")
	public String virusNews() {
		return "modular/business/virus/news.html";
	}

	@GetResource(name = "防护知识", path = "virusProtection")
	public String protection() {
		return "modular/business/virus/protection.html";
	}



	/**
	 * 破解工具集
	 * @return
	 */

	@GetResource(name = "破解工具集", path = "tools")
	public String virusTools() {
		return "modular/business/virus/tools.html";
	}


	/**
	 * 结构分析报告
	 * @return
	 */

	@GetResource(name = "结果分析报告", path = "virusReport")
	public String reports() {
		return "modular/business/virus/reports.html";
	}


	/**
	 * 下载系统预制的系统工具集
	 * @param fileName
	 */
	/*
	 * @GetMapping(name = "下载工具集", value = "tools/download") public void
	 * downloadFiles(@RequestParam("filename") String fileName) {
	 * HttpServletResponse response = HttpServletUtil.getResponse();
	 * service.downloadTools(response,fileName); }
	 */

	/**
	 * 下载应用
	 *
	 * @author 
	 * @date 2020/11/29 11:19
	 */
	/*
	 * @PostResource(name = "下载应用", path = "/tools/download", requiredPermission =
	 * false) public ResponseData download(@RequestParam String path) { File file =
	 * new File(path); HttpServletResponse response = HttpServletUtil.getResponse();
	 * if (file.exists() && file.isFile()) { byte[] bytes =
	 * FileUtil.readBytes(file.getAbsolutePath());
	 * DownloadUtil.download(file.getName(), bytes, response); } else {
	 * response.setStatus(404); }
	 * 
	 * return new SuccessResponseData(); }
	 */

	/**
	 * 查看结构报告
	 * @param reportPath
	 * @return 
	 */
	@PostResource(name = "查看结构报告", path = "/report/view", requiredPermission = false)
	public ResponseData viewReport(@RequestParam String reportPath) {
		service.viewReport(reportPath);
		return new SuccessResponseData();

	}

	/**
	 * 新闻数据拉取
	 * @return
	 */
	@GetMapping(name = "新闻加载", value = "news/load")
	@ResponseBody
	public ResponseData loadNews(@RequestParam("size") int size){


		return  service.loadNews(size);

	}


	/**
	 * 防护知识拉取
	 * @return
	 */
	@GetMapping(name = "防护知识拉取", value = "protection/load")
	@ResponseBody
	public ResponseData loadProtection(@RequestParam("size") int size ){


		return  service.loadProtection(size);

	}


	







}
