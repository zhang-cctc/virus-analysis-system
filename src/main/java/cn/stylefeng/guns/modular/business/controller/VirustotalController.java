package cn.stylefeng.guns.modular.business.controller;

import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import org.springframework.stereotype.Controller;

/**
 * <p> 
 *   规则管理页面跳转
 * </p>
 *
 *
 * @author lbw 
 * 2021年5月13日
 *
 */
@Controller
@ApiResource(name = "病毒样本分析Virustotal界面")
public class VirustotalController {

    @GetResource(name = "上传页面", path = "/view/virustotal/upload", requiredPermission = false)
    public String upload() {
    	return "modular/business/virustotal/upload.html";
    }
    @GetResource(name = "上传页面", path = "/view/virustotal/show", requiredPermission = false)
    public String show() {
    	return "modular/business/virustotal/show.html";
    }
   
}
