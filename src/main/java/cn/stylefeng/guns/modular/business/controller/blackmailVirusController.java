package cn.stylefeng.guns.modular.business.controller;

import cn.stylefeng.roses.kernel.scanner.api.annotation.ApiResource;
import cn.stylefeng.roses.kernel.scanner.api.annotation.GetResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 文件管理界面
 *
 * @author lgq
 * @date 2021/1/9
 */
@Controller
@ApiResource(name = "勒索病毒分析界面")
public class blackmailVirusController {

    
    /**  
     * <p> 
         *   勒索病毒分析界面
     * </p>
     * 	@author 	    kqm
     * 	@date 			2021年5月11日 上午11:38:01
     * 	@version V3.1  
     *	@return	
     */ 
    @GetResource(name = "勒索病毒破解和防护", path = "/view/blackmailVirusCP")
    public String blackmailVirusCP() {
    	return "modular/business/blackmailVirus/blackmailVirusCP.html";
    }
    @GetResource(name = "勒索病毒密码结构分析", path = "/view/blackmailVirusACS")
    public String blackmailVirusACS() {
    	return "modular/business/blackmailVirus/blackmailVirusACS.html";
    }
}
