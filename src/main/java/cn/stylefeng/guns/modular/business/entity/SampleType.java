package cn.stylefeng.guns.modular.business.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import cn.stylefeng.roses.kernel.db.api.pojo.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

//类别管理
@EqualsAndHashCode(callSuper = true)
@Data
@TableName("sample_type")
public class SampleType extends BaseEntity {
	
	private static final long serialVersionUID = 1L;
	/**
	 * 主键ID
	 */
	  @TableId(value = "type_id", type = IdType.ASSIGN_ID)
    private Long typeId;

	@TableField("type_name")
    private String typeName;
	@TableField("type_english")
    private String typeEnglish;
	@TableField("type_note")
    private String typeNote;
	@TableField("create_time")
    private Date createTime;
	@TableField("create_user")
    private Long createUser;
	@TableField("update_time")
    private Date updateTime;
	@TableField("update_user")
    private Long updateUser;

	/*
	 * public Long getTypeId() { return typeId; }
	 * 
	 * public void setTypeId(Long typeId) { this.typeId = typeId; }
	 * 
	 * public String getTypeName() { return typeName; }
	 * 
	 * public void setTypeName(String typeName) { this.typeName = typeName == null ?
	 * null : typeName.trim(); }
	 * 
	 * public String getTypeEnglish() { return typeEnglish; }
	 * 
	 * public void setTypeEnglish(String typeEnglish) { this.typeEnglish =
	 * typeEnglish == null ? null : typeEnglish.trim(); }
	 * 
	 * public String getTypeNote() { return typeNote; }
	 * 
	 * public void setTypeNote(String typeNote) { this.typeNote = typeNote == null ?
	 * null : typeNote.trim(); }
	 * 
	 * public Date getCreateTime() { return createTime; }
	 * 
	 * public void setCreateTime(Date createTime) { this.createTime = createTime; }
	 * 
	 * public Long getCreateUser() { return createUser; }
	 * 
	 * public void setCreateUser(Long createUser) { this.createUser = createUser; }
	 * 
	 * public Date getUpdateTime() { return updateTime; }
	 * 
	 * public void setUpdateTime(Date updateTime) { this.updateTime = updateTime; }
	 * 
	 * public Long getUpdateUser() { return updateUser; }
	 * 
	 * public void setUpdateUser(Long updateUser) { this.updateUser = updateUser; }
	 */
}