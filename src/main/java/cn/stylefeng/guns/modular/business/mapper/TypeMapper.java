package cn.stylefeng.guns.modular.business.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import cn.stylefeng.guns.modular.business.entity.Rule;
import cn.stylefeng.guns.modular.business.entity.SampleType;
import cn.stylefeng.guns.modular.business.entity.TypeExample;

public interface TypeMapper extends BaseMapper<SampleType> {
   
    int deleteByPrimaryKey(Long typeId);


    int insertSelective(SampleType record);

    SampleType selectByPrimaryKey(Long typeId);

    int updateByPrimaryKeySelective(SampleType record);

    int updateByPrimaryKey(SampleType record);

	List<SampleType> TypeList(@Param("page") Page<SampleType> page, @Param("type")SampleType type);
	
	SampleType selectBytypeName( @Param("type")SampleType type);
}