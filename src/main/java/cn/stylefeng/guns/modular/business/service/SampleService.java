/*
 * Copyright [2020-2030] [https://www.stylefeng.cn]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Guns采用APACHE LICENSE 2.0开源协议，您在使用过程中，需要注意以下几点：
 *
 * 1.请不要删除和修改根目录下的LICENSE文件。
 * 2.请不要删除和修改Guns源码头部的版权声明。
 * 3.请保留源码和相关描述文件的项目出处，作者声明等。
 * 4.分发源码时候，请注明软件出处 https://gitee.com/stylefeng/guns
 * 5.在修改包名，模块名称，项目代码等时，请注明软件出处 https://gitee.com/stylefeng/guns
 * 6.若您的项目无法满足以上几点，可申请商业授权
 */
package cn.stylefeng.guns.modular.business.service;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;

import cn.stylefeng.guns.modular.business.entity.Sample;
import cn.stylefeng.roses.kernel.db.api.pojo.page.PageResult;

/**
 * 文件信息表 服务类
 *
 * @author stylefeng
 * @date 2020/6/7 22:15
 */
public interface SampleService extends IService<Sample> {

	String testCuckoo();  
 
	Sample getSampleResult(Long fileId);



	Sample uploadFile(MultipartFile file);
	
	/**  
	 * <p> 
	 *   添加样本（包含上传文件）
	 * </p>
	 * 	@author 	    lbw
	 * 	@date 			2021年5月11日 上午10:59:32
	 *	@param file
	 *	@param sample
	 *	@return	sample
	 */ 
	Sample analysisSample(Sample sample) throws IOException;
	
	/**  
	 * <p> 
	 *   根据任务id查看html报告
	 * </p>
	 * 	@author 	    lbw
	 * 	@date 			2021年5月12日 上午10:33:01
	 *	@param taskID
	 *	@return	
	 */ 
	String viewReport(long taskID);
	
	/**  
	 * <p> 
	 *   根据任务id查看详情
	 * </p>
	 * 	@author 	    lbw
	 * 	@date 			2021年5月14日 下午3:53:03
	 * 	@version V3.1  
	 *	@param taskID
	 *	@return	
	 */ 
	String getTaskStatus(long taskID);
	
	/**  
	 * <p> 
	 *   根据任务id下载报告
	 * </p>
	 * 	@author 	    lbw
	 * 	@date 			2021年5月12日 上午10:34:16
	 *	@param taskID
	 *	@param response	
	 */ 
	void downloadReport(long taskID, HttpServletResponse response);

	void deleteReally(Sample sample);

	PageResult<Sample> sampleListPage(Sample sample);

	void packagingDownload(String fileIds, HttpServletResponse response);

	List<Sample> getSampleListByFileIds(String fileIds);





	Sample update(MultipartFile file, Sample sample);



	Sample detail(long sampleId);
	/**
	 * 样本类型百分比
	 * @return
	 */
	JSONArray staticTypePer();
	/**
	 * 样本类型排行
	 * @return
	 */
	JSONObject staticTypeTop();
	/**
	 * 样本添加趋势
	 * @return
	 */
	JSONObject staticFileCount();

	/**
	 *创建Sample
	 */
	Boolean addSampleByFile(MultipartFile file) throws IOException;

	/**
	 *创建Sample
	 * @throws Exception 
	 */
	Sample addSampleAndFile(MultipartFile file, Sample sample) throws IOException, Exception;

	PageResult<Sample> sampleListPageByStatus0(Sample sample);

	PageResult<Sample> sampleListPageByStatus1(Sample sample);
}
