package cn.stylefeng.guns.modular.business.service;

import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;

import javax.servlet.http.HttpServletResponse;

/**
 * Decription: 
 * @author wangtao
 * @version 1.0
 * create on 2021/6/1.
 *
 */
public interface VirusService {
	/**
	 * 加载新闻信息
	 * @return
	 * @param size
	 */
	ResponseData loadNews(int size);




	/**
	 * 下载工具集
	 * @param response
	 * @param fileName
	 */
	void downloadTools(HttpServletResponse response, String fileName);

	/**
	 * 查看结构报告
	 * @param response
	 * @param fileName
	 */
	ResponseData viewReport(String reportPath);

	/**
	 * 防护知识拉取
	 * @return
	 * @param size
	 */
	ResponseData loadProtection(int size);




	
}
