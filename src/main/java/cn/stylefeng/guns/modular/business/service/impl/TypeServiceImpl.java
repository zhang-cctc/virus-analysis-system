package cn.stylefeng.guns.modular.business.service.impl;

import static cn.stylefeng.roses.kernel.file.api.constants.FileConstants.DEFAULT_BUCKET_NAME;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import cn.stylefeng.guns.modular.business.entity.SampleType;
import cn.stylefeng.guns.modular.business.mapper.TypeMapper;
import cn.stylefeng.guns.modular.business.service.TypeService;
import cn.stylefeng.guns.modular.business.utils.ScctcConst;
import cn.stylefeng.guns.modular.business.utils.WriteFileUtils;
import cn.stylefeng.roses.kernel.db.api.factory.PageFactory;
import cn.stylefeng.roses.kernel.db.api.factory.PageResultFactory;
import cn.stylefeng.roses.kernel.db.api.pojo.page.PageResult;
import cn.stylefeng.roses.kernel.file.api.FileOperatorApi;
import cn.stylefeng.roses.kernel.file.api.exception.FileException;
import cn.stylefeng.roses.kernel.file.api.exception.enums.FileExceptionEnum;
import cn.stylefeng.roses.kernel.file.api.pojo.response.SysFileInfoResponse;
import cn.stylefeng.roses.kernel.file.api.util.DownloadUtil;
import lombok.extern.slf4j.Slf4j;
@Service
@Slf4j
public class TypeServiceImpl extends ServiceImpl<TypeMapper, SampleType> implements TypeService {

	

	@Resource
    private FileOperatorApi fileOperatorApi;
	
	@Override
	public PageResult<SampleType> typeListPage(SampleType type) {
		
		 Page<SampleType> page = PageFactory.defaultPage();
        List<SampleType> list = this.baseMapper.TypeList(page,type);
        return PageResultFactory.createPageResult(page.setRecords(list));
	}


	@Override
	public SampleType selectBytypeName(SampleType type) {
		return this.baseMapper.selectBytypeName(type);
	}

	@Override
	public boolean save(SampleType type) {
		type.setCreateTime(new Date());
		return super.save(type);
	}
	

	/*
	 * @Override public void downloadTypes(HttpServletResponse response,String
	 * fileName) { // 根据文件名获取文件信息结果集 SysFileInfoResponse sysFileInfoResponse =
	 * this.getFileInfoResult( fileName); DownloadUtil.download(fileName,
	 * sysFileInfoResponse.getFileBytes(), response);
	 * 
	 * }
	 */
	
	

}
