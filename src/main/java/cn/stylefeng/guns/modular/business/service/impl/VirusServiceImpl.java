package cn.stylefeng.guns.modular.business.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.http.HttpUtil;
import cn.stylefeng.guns.modular.business.service.VirusService;
import cn.stylefeng.guns.modular.business.utils.ScctcConst;
import cn.stylefeng.roses.kernel.file.api.util.DownloadUtil;
import cn.stylefeng.roses.kernel.rule.pojo.response.ErrorResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.ResponseData;
import cn.stylefeng.roses.kernel.rule.pojo.response.SuccessResponseData;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Decription: 
 * @author wangtao
 * @version 1.0
 * create on 2021/6/1.
 *
 */
@Service

public class VirusServiceImpl implements VirusService {


	@Override
	public ResponseData loadNews(int size) {

		String result2 = HttpUtil.get(ScctcConst.NEWS_URL, CharsetUtil.CHARSET_UTF_8);
		Document doc = Jsoup.parse(result2);
		Elements elements = doc.getElementsByTag("li");

		JSONArray msgArray = new JSONArray();
		for (int i = 0; i < elements.size(); i++) {

			JSONObject object = new JSONObject();
			String href = elements.get(i).child(0).attr("href");
			String title = elements.get(i).child(0).text();
			object.put("href", ScctcConst.VIEW_URL_PREFIX + href);
			object.put("title", title);
			object.put("id", i);
			msgArray.add(object);

		}

		if (!msgArray.isEmpty()) {
			List<Object> subList = msgArray.subList(0, Math.min(msgArray.size(),size));
			return new SuccessResponseData(subList);
		}else{
			return new SuccessResponseData(msgArray);
		}


	}



	@Override
	public void downloadTools(HttpServletResponse response, String fileName) {

		File file = new File(ScctcConst.VIRUS_TOOLS_SPATH + File.separator + fileName);
		if (file.exists() && file.isFile()) {
			byte[] bytes = FileUtil.readBytes(file.getAbsolutePath());
			DownloadUtil.download(fileName, bytes, response);
		} else {
			response.setStatus(404);
		}

	}

	@Override
	public ResponseData viewReport(String reportPath) {


		File file = new File(reportPath);
		if (file.exists() && file.isFile()) {
			try {
				Runtime.getRuntime().exec("rundll32 url.dll FileProtocolHandler " + file.getAbsolutePath());

				return new SuccessResponseData();
			} catch (IOException e) {
				return  new ErrorResponseData("500","文件打开失败");
			}
		} else {
			return  new ErrorResponseData("404","报告文件不存在");

		}
	}

	@Override
	public ResponseData loadProtection(int size) {
		String result2 = HttpUtil.get(ScctcConst.PROTECTION_URL, CharsetUtil.CHARSET_UTF_8);

		Document doc = Jsoup.parse(result2);
		Elements elements = doc.getElementsByAttributeValue("class","container-center");

		Elements elements1 = elements.get(0).getElementsByAttributeValue("class", "title-left");
		Elements elements2 = elements.get(0).getElementsByAttributeValue("class", "item-right");
		JSONArray array = new JSONArray();
		for(int i=0;i<elements1.size();i++){

			Element element = elements1.get(i);

			Element context=elements2.get(i).select("a").first();


			JSONObject object = new JSONObject();
			object.put("title",element.html().replace("/articles/es",ScctcConst.PROTECTION_URL));
			object.put("context",context.html());

			array.add(object);



		}

		if(!array.isEmpty()){
			List<Object> subList = array.subList(0, Math.min(size,array.size()));
			return new SuccessResponseData(subList);
		}else{
			return new SuccessResponseData(array);
		}







	}
}
