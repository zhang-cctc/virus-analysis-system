package cn.stylefeng.guns.modular.business.utils;

import static cn.stylefeng.roses.kernel.file.api.constants.FileConstants.DEFAULT_BUCKET_NAME;

/**
 * <p>
 * 国家商用密码中心初始化常量类
 * </p>
 *
 *
 * @author lbw 2021年5月12日
 *
 */
public class ScctcConst {

	/** 代码Cukoo访问地址和端口 */
	public static String CUCKOO_URL = "http://192.168.8.106:8090";

	/**
	 * 本地文件存储位置（linux）
	 */
	public static String FILE_PATH_LINUX = "/tmp/tempFilePath";

	/**
	 * 本地文件存储位置（windows）
	 */
	public static String FILE_PATH_WIN = "C:/CuckooFile";

	/**
	 * 存值位置默认为windows环境 且配置值要与数据库保持一致。
	 */
	public static String FILE_PATH = FILE_PATH_WIN;

	/**
	 * 导出规则名字
	 */
	public static String EXPORTR_RULE_NAME = "export.rules";

	/**
	 * 新闻列表
	 */
	public static final String NEWS_URL = "http://www.cverc.org.cn/head/lesuoruanjian/mulu.htm";

	/**
	 * 新闻详情
	 */
	public static final String VIEW_URL_PREFIX = "http://www.cverc.org.cn/head/lesuoruanjian/";

	/**
	 * 防护知识
	 */
	public static final String PROTECTION_URL = "https://www.freebuf.com/articles/es";

	/**
	 * 勒索病毒破解工具集存储路径
	 */
	public static final String VIRUS_TOOLS_SPATH = "C:/CuckooFile/virus/tools";

	/**
	 * 勒索病毒结构分析报告存储路径
	 *//*
		 * public static final String VIRUS_REPORT_SPATH="C:/CuckooFile/virus/reports";
		 */
}
