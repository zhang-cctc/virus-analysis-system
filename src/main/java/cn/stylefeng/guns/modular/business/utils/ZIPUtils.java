package cn.stylefeng.guns.modular.business.utils;


import java.io.*;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * 文件压缩工具类
 */
public class ZIPUtils {
        /**
         * 先根遍历序递归删除文件夹
         *
         * @param dirFile 要被删除的文件或者目录
         * @return 删除成功返回true, 否则返回false
         */
        public static boolean deleteFile(File dirFile) {
            // 如果dir对应的文件不存在，则退出
            if (!dirFile.exists()) {
                return false;
            }

            if (dirFile.isFile()) {
                return dirFile.delete();
            } else {

                for (File file : dirFile.listFiles()) {
                    deleteFile(file);
                }
            }

            return dirFile.delete();
        }
    /**
     * 解压文件到指定目录
     */
    public static void unZipFiles(File zipFile, String descDir) throws IOException {
        try{
            if(!zipFile.exists()){
                throw new IOException("需解压文件不存在.");
            }
            ZipFile zip = new ZipFile(zipFile, Charset.forName("GBK"));
            for (Enumeration entries = zip.entries(); entries.hasMoreElements();) {
                ZipEntry entry = (ZipEntry) entries.nextElement();
                String zipEntryName = entry.getName();
                System.err.println(zipEntryName);
                InputStream in = zip.getInputStream(entry);


                String outPath = (descDir + File.separator + zipEntryName).replaceAll("\\*", "/");
                System.err.println(outPath);
                // 判断路径是否存在,不存在则创建文件路径
//                File file = new File(outPath.substring(0, outPath.lastIndexOf('/')));
//                if (!file.exists()) {
//                    file.mkdirs();
//                }
                // 判断文件全路径是否为文件夹,如果是上面已经上传,不需要解压
                if (new File(outPath).isDirectory()) {
                    continue;
                }
                // 输出文件路径信息
                OutputStream out = new FileOutputStream(outPath);
                byte[] buf1 = new byte[1024];
                int len;
                while ((len = in.read(buf1)) > 0) {
                    out.write(buf1, 0, len);
                }
                in.close();
                out.close();
            }
        }catch(Exception e){
            throw new IOException(e);
        }
    }

    // 删除文件或文件夹以及文件夹下所有文件
    public static void delDir(String dirPath) throws IOException {
        long start = System.currentTimeMillis();
        try{
            File dirFile = new File(dirPath);
            if (!dirFile.exists()) {
                return;
            }
            if (dirFile.isFile()) {
                dirFile.delete();
                return;
            }
            File[] files = dirFile.listFiles();
            if(files==null){
                return;
            }
            for (int i = 0; i < files.length; i++) {
                delDir(files[i].toString());
            }
            dirFile.delete();
        }catch(Exception e){
            throw new IOException("删除文件异常.");
        }
    }
}

