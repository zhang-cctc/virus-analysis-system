layui.use(['layer', 'form', 'admin', 'HttpRequest'], function () {
    var $ = layui.$;
    var form = layui.form;
    var HttpRequest = layui.HttpRequest;
	var upload = layui.upload;
    //获取详情信息，填充表单
    var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/viewreport?sampleId=" + Feng.getUrlParam("sampleId"),'get');
    var result = httpRequest.start();
    if(result == ""){
    	Feng.error("操作失败！任务尚未分析完成。" )
    	return ;
    }
    if(!result.success){
    	Feng.error("操作失败！" +result.message)
    	return ;
    }
    var data =JSON.parse(result.data);    
    document.getElementById("targetName").innerHTML=data.target.file.name;
    document.getElementById("size").innerHTML=data.target.file.size;
    document.getElementById("type").innerHTML=data.target.file.type;
    document.getElementById("md5").innerHTML=data.target.file.md5;
    document.getElementById("sha1").innerHTML=data.target.file.sha1;
    document.getElementById("sha256").innerHTML=data.target.file.sha256;
//    document.getElementById("sha512").innerHTML=data.target.file.sha512;
    document.getElementById("crc32").innerHTML=data.target.file.crc32;
//    document.getElementById("ssdeep").innerHTML=data.target.file.ssdeep;
//    document.getElementById("yara").innerHTML=data.target.file.yara;
    var logs=[];
    if(data.debug.log.length==0){
    	logs.push("暂无日志信息");
    }else{
    	logs=data.debug.log;
    }
    for(var i in logs){
    	var element = 	document.createElement("p");
    	
    	var b=element.nodeName;
    	var v=element.nodeType;
    	
    	document.querySelector(".logInfo").appendChild(element);     //添加到body 子节点下
    	element.innerHTML=logs[i]; 
    	
    	
    }
   // document.querySelector(".logInfo").innerHTML=logs;
    console.log(data.debug.log);
    
//    document.getElementById("img_sample_view").src = Feng.ctxPath+"/assets/modular/business/blackmailVirus/111.png";
    /*var SampleView = {
        tableId: "sampleViewTable"
    };*/
 
});
