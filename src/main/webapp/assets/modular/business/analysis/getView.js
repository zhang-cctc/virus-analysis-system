layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var form = layui.form;
    var func = layui.func;
    var HttpRequest = layui.HttpRequest;
    var util = layui.util;
    var upload = layui.upload;
    var layer = layui.layer;

    var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/getCuckooUrl",'get');
    var result = httpRequest.start();
    var cuckooUrl = result.data;
    // 样本管理
    var GetView = {
        tableId: "getViewTable"
    };

    // 初始化表格的列
    GetView.initColumn = function () {
        return [[
            {field: 'xuhao', width:70,sort: true, title: '序号',type:'numbers'},
            {field: 'sampleId', hide: true, title: '主键id'},
            {field: 'sampleZhName', sort: true, title: '样本名称'},
            {field: 'typeName', sort: true, title: '样本类别'},
            {field: 'sampleType', sort: true, title: '病毒家族'},
            {field: 'sampleSuffix', sort: true, title: '样本后缀'},
            {field: 'sampleOriginName', sort: true, title: '文件名称'},
            {field: 'sampleSizeKb', sort: true, title: '样本大小-KB'},
            {
                field: 'updateTime', sort: true, title: '分析时间', templet: function (d) {
                	return util.toDateString(d.updateTime, 'yyyy年MM月dd日');
                }
            },
           
            {align: 'center', toolbar: '#getViewBar', title: '操作', width: 150},
        ]];
    };


    // 点击查询按钮
    GetView.search = function () {
        var queryData = {};
        queryData['sampleZhName'] = $("#sampleZhName").val();
        queryData['typeName'] = $("#typeName").val();
        //queryData['positionCode'] = $("#positionCode").val();
        table.reload(GetView.tableId, {
            where: queryData,
            page: {curr: 1}
        });
    };

    // 渲染表格
    var tableResult = table.render({
    	initSort: {
            field: 'updateTime' //排序字段，对应 cols 设定的各字段名
            ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
          },
        elem: '#' + GetView.tableId,
        //只返回状态为1的已分析的对象
        url: Feng.ctxPath + '/Sample/sampleListPageByStatus1',
        page: true,
        request: {pageName: 'pageNo', limitName: 'pageSize'}, //自定义分页参数
        height: "full-158",
        cellMinWidth: 100,
        cols: GetView.initColumn(),
        parseData: Feng.parseData
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        GetView.search();
    });

    // 工具条点击事件
    table.on('tool(' + GetView.tableId + ')', function (obj) {
    	
        var data = obj.data;
        var event = obj.event;
        if (event === 'lookLog') {
        	GetView.lookLog(data);
        } 
    });

//  弹出具体信息页面
    GetView.lookLog = function (data) {
        func.open({
            title: '分析摘要',
            content: Feng.ctxPath + '/view/analysis/getDetail?sampleId='+data.sampleId,
            tableId: GetView.tableId
        });
    };
  
    
    
   

   
});
