layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var form = layui.form;
    var func = layui.func;
    var HttpRequest = layui.HttpRequest;
    var util = layui.util;
    var upload = layui.upload;
    var layer = layui.layer;

    var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/getCuckooUrl",'get');
    var result = httpRequest.start();
    var cuckooUrl = result.data;
    // 样本管理
    var ViewReport = {
        tableId: "viewReportTable"
    };

    // 初始化表格的列
    ViewReport.initColumn = function () {
        return [[
                 {field: 'xuhao', width:70,sort: true, title: '序号',type:'numbers'},
            {field: 'sampleId', hide: true, title: '主键id'},
            {field: 'sampleZhName', sort: true, title: '样本名称'},
            {field: 'typeName', sort: true, title: '样本类别'},
            {field: 'sampleType', sort: true, title: '病毒家族'},
           /* {field: 'samplePath', sort: true, title: '存储位置'},       */    
            {field: 'sampleSuffix', sort: true, title: '样本后缀'},
            {field: 'sampleOriginName', sort: true, title: '文件名称'},
            {field: 'sampleSizeKb', sort: true, title: '样本大小-KB'},
            {
                field: 'updateTime', sort: true, title: '分析时间', templet: function (d) {
                	return util.toDateString(d.updateTime, 'yyyy年MM月dd日');
                }
            },
           
            {align: 'center', toolbar: '#viewReportBar', title: '操作', width: 250},
        ]];
    };


    // 点击查询按钮
    ViewReport.search = function () {
        var queryData = {};
        queryData['typeName'] = $("#typeName").val();
        queryData['sampleZhName'] = $("#sampleZhName").val();
        //queryData['positionCode'] = $("#positionCode").val();
        table.reload(ViewReport.tableId, {
            where: queryData,
            page: {curr: 1}
        });
    };

    // 渲染表格
    var tableResult = table.render({
    	   initSort: {
               field: 'updateTime' //排序字段，对应 cols 设定的各字段名
               ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
             },
        elem: '#' + ViewReport.tableId,
        //只返回状态为0的未分析的对象
        url: Feng.ctxPath + '/Sample/sampleListPageByStatus1',
        page: true,
        request: {pageName: 'pageNo', limitName: 'pageSize'}, //自定义分页参数
        height: "full-158",
        cellMinWidth: 100,
        cols: ViewReport.initColumn(),
        parseData: Feng.parseData,
     
        
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        ViewReport.search();
    });

    // 工具条点击事件
    table.on('tool(' + ViewReport.tableId + ')', function (obj) {
    	
        var data = obj.data;
        var event = obj.event;
        if (event === 'sampleDetails') {
        	ViewReport.openDetails(data);
        } else if (event === 'delete') {
        	ViewReport.onDeleteSample(data);
        } else if (event === 'lookReport') {
        	console.log(1213);
        	ViewReport.lookReport(data);
		} else if (event === "downloadReport") {
			ViewReport.downloadReport(data);
		}
    });

   
    /**
     * 查看报告
     */
    ViewReport.lookReport = function (data) {
    	console.log(11);
    	if (data.taskId === '' || data.taskId == null) {
  		  Feng.error("请先执行分析操作！");
  		  
		}else {
			console.log(11);
			window.open(cuckooUrl  +'/tasks/report/'+ data.taskId+'/html');
		}
    };
    
    /**
     * 下载报告
     */
    ViewReport.downloadReport = function (data) {
    	if (data.taskId === '' || data.taskId == null) {
    		  Feng.error("正在分析中，请稍候！");
		}else {
			var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/viewreport?sampleId=" + data.sampleId,'get');
		    var result = httpRequest.start();
		    if(result == ""){
		    	Feng.error("正在分析中，请稍候！" )
		    	return ;
		    }
		    if(!result.success){
		    	Feng.error("操作失败！" +result.message)
		    	return ;
		    }
			window.location.href = cuckooUrl  +'/tasks/report/'+ data.taskId+'/all';
//			var curWwwPath = window.document.location.href;
//	        var pathName = window.document.location.pathname;
//	        var pos = curWwwPath.indexOf(pathName);
//	        var localhostPath = curWwwPath.substring(0, pos);
//	        var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
//	        var url = localhostPath + projectName + "/assets/common/images/bg-login.jpg";
//	        window.open(url);
	
//			window.location.href = 'http://192.168.8.108:8080/assets/common/images/bg-login.jpg';
		}
    };

   
});
