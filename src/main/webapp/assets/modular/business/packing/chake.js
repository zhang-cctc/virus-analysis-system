layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var chake = {
        tableId: "chakeTable"
    };

    // 渲染表格
    var data = [
{
	"name":"AddPEbytesv0.1.1",
	 "msg":"AddPEbytes是一个程序加区段工具，用来添加区段，扩展任何区段（包含PEHEADER），捆绑任何文件到程序指定的位置。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/Add PE bytes v0.1.1/apf011.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/AddPEbytesv0.1.1.zip"
	},
	{
	"name":"CFF_Explorer",
	 "msg":"是一款pe编辑器软件，用来修改EXE和DLL文件，集合了字段描述、修改、实用程序、重建、十六进制编辑器、反汇编、资源编辑等多种功能，值得一提的是CFFExplorer还支持脱壳后清理不必要的代码等。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/CFF_Explorer/CFF Explorer.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/CFF_Explorer.zip"
	},
	{
	"name":"CHimpREC",
	 "msg":"是第一款支持64位系统的输入表重建工具，同时也支持32位。有着和ImportREC相似的功能和界面。功能：32/64位进程转存功能、从基址或者OEP自动搜索IAT、反移指针功能、手动输入表编辑器；限制：尚未支持插件、没有自动追踪功能、没有反编译器。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/CHimpREC/CHimpREC汉化.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/CHimpREC.zip"
	},
	{
	"name":"DetectItEasyv1.0",
	 "msg":"DetectItEasy是一个多功能的PE检测工具，基于QT平台编写，主要用于PE是否加壳侦测。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/DetectItEasy v1.0/die.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/DetectItEasyv1.0.zip"
	},
	{
	"name":"PEAddData",
	 "msg":"PE文件修复工具，可以对PE文件进行添加数据等操作。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/EAddData/EAddData.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/EAddData.zip"
	},
	{
	"name":"EnigmaInfov0.11",
	 "msg":"是一款免费的Enigma版本查询工具，主要用于获得受保护文件的详细信息，包括版本、日期、水印、许可证类型等。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/EnigmaInfo v0.11/EnigmaInfo.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/EnigmaInfov0.11.zip"
	},
	{
	"name":"exeinfoPe",
	 "msg":"是一款查看PE文件信息的工具，可以查看EXE/DLL文件的编译器信息、是否加壳、入口点地址、输出表/输入表等等PE信息，帮助开发人员对程序进行分析和逆向。还可以提取PE文件中的资源，可以提取图片、EXE、压缩包、MSI、SWF等资源。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/ExeinfoPe/exeinfope.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/CuckooFile/packing/Tools/PETools/infoPe.zip"
	},
	{
	"name":"FFI",
	 "msg":"FFI	语言交互接口(ForeignFunctionInterface)，一个可以在某种计算机语言中调用其它语言的接口。由于现实中很多程序是由不同编程语言写的，必然会涉及到跨语言调用，比如A语言写的函数如果想在B语言里面调用，这时一般有两种解决方案：一种是将函数做成一个服务，通过进程间通信(IPC)或网络协议通信(RPC,RESTful等)；另一种就是直接通过FFI调用。前者需要至少两个独立的进程才能实现，而后者直接将其它语言的接口内嵌到本语言中，所以调用效率比前者高。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/FFI/FFI.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/FFI.zip"
	},
	{
	"name":"GeTaOEP",
	"msg":"GeTaOEP是一款OEP查找工具，能快速识别软件的OEP，是脱壳和逆向工程中需要的工具",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/Get_approximate_OEP_ver_1.0.1.1/GeTaOEP.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/Get_approximate_OEP_ver_1.0.1.1.zip"
	},
	{
	"name":"Import_REC1.7_iCrack",
	"msg":"是一种PE文件输入表修复工具，和OD,LordPE配合使用，PE文件脱壳更方便。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/Import_REC1.7_iCrack专版/ImportREC.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/Import_REC1.7_iCrack专版.zip"
	},
	{
	"name":"ImpRECFINAL1.7",
	"msg":"ImportREConstructor可以从杂乱的IAT中重建一个新的Import表(例如加壳软件等)，它可以重建Import表的描述符、IAT和所有的ASCII函数名。",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/ImpREC FINAL 1.7汉化版/ImpREC FINAL 1.7汉化版.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/ImpRECFINAL1.7汉化版.zip"
	},
	/*{
	"name":"LordPE.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/LordPE/LordPE吾爱破解专用版.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/LordPE.zip"
	},
	{
	"name":"LordPE_iCrack.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/LordPE_iCrack/Misc/SoftSnoop/SoftSnoop/SoftSnoop.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/LordPE_iCrack.zip"
	},
	{
	"name":"oepfinder.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/oepfinder/bin/oep查找.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/oepfinder.zip"
	},
	{
	"name":"PEOptimizerV1.4.zip",
	"msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/PE Optimizer V1.4/PE Optimizer V1.4 汉化版.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/PEOptimizerV1.4.zip"
	},
	{
	"name":"PEditor1.7.zip",
	"msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/PEditor 1.7/PEditor.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/PEditor1.7.zip"
	},
	{
	"name":"PEiD0.95.zip",
	"msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/PEiD 0.95/PEID V0.95.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/PEiD0.95.zip"
	},
	{
	"name":"PETools.zip",
	"msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/PETools/PETools.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/PETools.zip"
	},
	{
	"name":"ReloREC.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/ReloREC/ReloREC.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/ReloREC.zip"
	},*/
	/*{
	"name":"ReloX.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/ReloX/ReloX.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/ReloX.zip"
	},
	{
	"name":"Resfixerv1.1.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/Resfixer v1.1/Resfixer.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/Resfixerv1.1.zip"
	},
	{
	"name":"ResourceBinder2.6CNFixed2.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/Resource Binder 2.6 CN Fixed2/Resource Binder 2.6 CN Fixed.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/ResourceBinder2.6CNFixed2.zip"
	},
	{
	"name":"ResourceBinder3.1字体修复版.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/Resource Binder 3.1字体修复版/ResBinder.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/ResourceBinder3.1字体修复版.zip"
	},
	{
	"name":"ScanItv1.85汉化版.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/ScanIt v1.85 汉化版/ScanIt.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/ScanItv1.85汉化版.zip"
	},
	{
	"name":"Scylla_v0.9.7b.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/Scylla_v0.9.7b/Scylla_x64.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/Scylla_v0.9.7b.zip"
	},
	{
	"name":"StudyPE+x64.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/StudyPE+x64/StudyPE+ x64.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/StudyPE+x64.zip"
	},*/
	/*{
	"name":"xPELister.V3.0汉化版.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/xPELister.V3.0汉化版/xPELister 3.0汉化版.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/xPELister.V3.0汉化版.zip"
	},
	{
	"name":"zeroadd1.0.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/zeroadd 1.0/zeroadd.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/zeroadd1.0.zip"
	},
	{
	"name":"ZPFixer.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/ZP Fixer/ZPFixer.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/ZPFixer.zip"
	},
	{
	"name":"基址偏移量转换器.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/基址偏移量转换器/基址偏移量转换器.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/基址偏移量转换器.zip"
	},
	{
	"name":"文件补区段工具.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/文件补区段工具/SesnAddor.exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/文件补区段工具.zip"
	},
	{
	"name":"通用输入表修复工具UIFv1.2FINAL汉化版.zip",
	 "msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/PETools/通用输入表修复工具 UIF v1.2 FINAL 汉化版/UIF v1.2_CN(英文标题).exe",
	"downloadPath":"C/CuckooFile/packing/Tools/PETools/通用输入表修复工具UIFv1.2FINAL汉化版.zip"
	},*/
                

    ];
    chake.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#chakeBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#chakeTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: chake.initColumn(),
        data: data,
        limit: 30
    });
    table.on('tool(' + chake.tableId + ')', function (obj) {

        var data = obj.data;
        
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(chake.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
        if (event === 'download') {
     	   
        	
          
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

