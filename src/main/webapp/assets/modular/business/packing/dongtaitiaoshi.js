layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var dongtaitiaoshi = {
        tableId: "dongtaitiaoshiTable"
    };

    // 渲染表格
    var data = [
{
	"name":"Cheat Engine 6.5",
	"msg":"是一款内存修改编辑工具，它包括16进制编辑，反汇编程序，内存查找工具。与同类修改工具相比，它具有强大的反汇编功能，且自身附带了外挂制作工具，可以用它直接生成外挂。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/Cheat Engine 6.5/Cheat Engine.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/Cheat Engine 6.5.zip",
	},
	{
	"name":"MDebug",
	"msg":"MDebug是一个支持脚本语言、功能丰富的反汇编调试器，可调试程序、进程和代码。MDebug脚本语言是类似C/C++的64位高级语言，其编译与执行引擎内置于MDebug调试器。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/MDebug x64/MDebug_x64.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/MDebug x64.zip",
	},
	{
	"name":"Nanomite",
	"msg":"Nanomite纳米米特，是一款图形调试器，使用C++实现，因此能支持X64和X86系统的调试操作。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/Nanomite x64/DebugMe.NET.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/Nanomite x64.zip",
	},
	{
	"name":"ollydbg",
	"msg":"是专门为调试多线程程序而构建的软件，能够执行代码分析，并显示有关寄存器、循环、api调用、交换机和许多其他方面的信息。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/OllyDbg v2.01/ollydbg.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/OllyDbg v2.01.zip",
	},
	{
	"name":"ollyICE专版",
	"msg":"ollyice是Windows平台下一款知名的反汇编工具软件，目前已经代替SoftICE成为当今最为流行的调试解密工具之一，同时，ollyice还支持插件扩展功能，最擅长分析函数过程、循环语句、选择语句、表、常量、代码中的字符串、欺骗性指令、API调用、函数中参数的数目和import表等等，这些分析增加了二进制代码的可读性，减少了出错的可能性，使得我们的调试工作更加容易。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/OllyICE 吾爱扣扣专版/ollydbg.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/OllyICE 吾爱扣扣专版.zip",
	},
	{
	"name":"OllyICE",
	"msg":"ollyice是Windows平台下一款知名的反汇编工具软件，目前已经代替SoftICE成为当今最为流行的调试解密工具之一，同时，ollyice还支持插件扩展功能，最擅长分析函数过程、循环语句、选择语句、表、常量、代码中的字符串、欺骗性指令、API调用、函数中参数的数目和import表等等，这些分析增加了二进制代码的可读性，减少了出错的可能性，使得我们的调试工作更加容易。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/OllyICE_1.10/OllyICE.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/OllyICE_1.10.zip",
	},
	{
	"name":"SimpleAssemblyExplorer",
	"msg":"是一款用于.NEt反编译、修改.NET程序的工具。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/SimpleAssemblyExplorer v1.10.1/SimpleAssemblyExplorer.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/SimpleAssemblyExplorer v1.10.1.zip",
	},
	{
	"name":"WinDbg",
	"msg":"我们在使用电脑时如果出现蓝屏情况，这时可以利用Windbg蓝屏分析修复工具来分析下蓝屏出现的原因，它通过对dmp文件分析、定位能快速帮你找到蓝屏问题并修复。使用Windbg蓝屏分析修复工具不一定保证能找到真正蓝屏的问题，只是辅助查找分析。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/WinDbg/x64/windbg.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/WinDbg.zip",
	},
	{
	"name":"Ollydbg中文版",
	"msg":"是专门为调试多线程程序而构建的软件，能够执行代码分析，并显示有关寄存器、循环、api调用、交换机和许多其他方面的信息。",
	"openPath":"C:/CuckooFile/packing/Tools/Debuggers/吾爱破解专用版Ollydbg/原版/汉化原版/Ollydbg.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Debuggers/吾爱破解专用版Ollydbg.zip",
	},
	{
		"name":"E-Debug Events",
		"msg":"是一款易语言反编译软件，能支持静态编译调试，还可以全面支持静态编译、独立编译、编译。E-DebugEvents在静态上增加了采用ReadPWholeMemory方式，支持加花指令调试,解决一个死循环代码。",
		"openPath":"C:\CuckooFile\packing\Tools\Debuggers\E-Debug Events V1.5.exe",
		"downloadPath":"C:\CuckooFile\packing\Tools\Debuggers.zip",
		},

    ];
    dongtaitiaoshi.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#dongtaitiaoshiBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#dongtaitiaoshiTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: dongtaitiaoshi.initColumn(),
        data: data
    });
    table.on('tool(' + dongtaitiaoshi.tableId + ')', function (obj) {

        var data = obj.data;
        var path = data.path;
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(dongtaitiaoshi.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
        if (event === 'download') {
     	   
               
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

