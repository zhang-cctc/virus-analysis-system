layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var fanhuibian = {
        tableId: "fanhuibianTable"
    };

    // 渲染表格
    var data = [
        {

            "name": "Dependency Walker",
            "msg": "是一款dll依赖查看工具，用户可以用这款软件对dll文件的修复和查看。这款软件非常适合用来分析pe模块，可以更好的进行dll的查看，是一款非常实用的动态链接库工具。这款软件处理可以查看PE模块的导入模块，还可以查看PE模块的导入和导出函数、动态剖析PE模块的模块依赖性和解析C++函数名称。这款软件还可以节省内存，解决应用本地化的问题，软件中还有非常实用的扩展应用程序，可以让用户在需要时才将DLL载入到内存中，这让程序的可维护性变得很高。",
            "openPath": "C:/CuckooFile/packing/DependencyWalker/depends22_x86/depends.exe",
            "downloadPath":"C:/CuckooFile/packing/DependencyWalker.zip"


        }, {

        	  "name": "IDA PRO",
              "msg": "是一款可编程、可扩展的交互式多处理器反汇编程序，也是典型的递归下降反汇编器，由强大的类宏语言组成，应该十分广泛，支持很多插件和python，在CTF中，就连逆向和pwn都少不了它。该软件支持数十种CPU指令集，其中包括Intelx86，x64，MIPS，PowerPC，ARM，Z80，68000，c8051等等，不仅使用数据类型信息，而且通过派生的变量和函数名称来尽其所能地注释生成的反汇编代码，这些注释可将原始十六进制代码的数量减到最少，并显著增加了向用户提供的符号化信息的数量，还采用了先进的逆向工程技术，能够帮助用户反向编译源代码，为科技型企业解决反编译相关问题。",
              "openPath": "C:/CuckooFile/packing/IDA_Pro_Green/IDA_PRO_7.5/ida64.exe",
              "downloadPath":"C:/CuckooFile/packing/IDA_Pro_Green.zip"


        }, {

        	  "name": "PEview",
              "msg": "是一款PE文件解析工具，采用汇编语言程序设计而来，可以查看dll、lib等库文件，显示详细的文件内容。",
              "openPath": "C:/CuckooFile/packing/peview/PEview.exe",
              "downloadPath":"C:/CuckooFile/packing/peview.zip"


        },{
        	"name":"C32Asm",
        	"msg":"是一款反汇编程序，具有反汇编和十六进制编辑两种模式，能够跟踪exe文件的断点，也可以直接修改软件内部代码，是逆向分析程序的必备软件，帮助用户轻松分析修改程序内容。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/C32Asm/C32Asm.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/C32Asm.zip",
        	},
        	{
        	"name":"hDasm64",
        	"msg":"是一款反汇编工具，该工具为用户提供了实时模式、保护模式、64位模式等模式指令集，支持对32位和64位的文件格式进行反汇编操作。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/dasm64/hDasm64.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/dasm64.zip",
        	},
        	{
        	"name":"DarkDe4",
        	"msg":"DeDeDark(Darkde4)是一款Delphi反编译工具,主要功能可帮助用户来进行delphi源代码的分析并进行部分界面的修改及反编译操作。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/DeDeDark/DarkDe4.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/DeDeDark.zip",
        	},
        	{
        	"name":"DelphiDecompiler",
        	"msg":"Delphi是全新的可视化编程环境，为我们提供了一种方便、快捷的Windows应用程序开发工具。DelphiDecompiler则是来自破解狂族俄罗斯的反编译软件DelphiDecompiler反编译Delphi生成的EXE，DLL，BPL，DCU等格式文件，软件旨在恢复部分由Delphi编译器丢失的源代码。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/DelphiDecompiler/DelphiDecompiler.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/DelphiDecompiler.zip",
        	},
        	{
        	"name":"E-Code Explorer",
        	"msg":"是一款反汇编调试工具，反汇编调试由易语言编译生成的易格式可执行文件，分析内部结构，查看其中的各项数据。可以帮助用户对易语言编写的软件程序进行拆包、修改以及反编译。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/E-Code Explorer 0.86/E-Code Explorer.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/E-Code Explorer 0.86.zip",
        	},
        	{
        	"name":"ETU-DASM",
        	"msg":"汇编编译器(ETU-DASM)是一个动态的32/16-bit反汇编器+Win9x和十六进制编辑器。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/ETU-Dasm/ETU-DASM.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/ETU-Dasm.zip",
        	},
        	
        	{
        	"name":"IDR",
        	"msg":"InteractiveDelphiReconstructor，这是一款Delphi工程反编译工具，通过它反编译出来的Delphi程序，你可以完整地看到Delphi的各个窗口，并且Delphi的按扭事件也能获取。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/IDR/Idr_CHS.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/IDR.zip",
        	},
        	{
        	"name":"SMARTCHK",
        	"msg":"SmartCheck(VB程序调试工具)是由NuMega公司推出的一款VB程序调试工具，SmartCheck可以自动检测和诊断VB运行时的错误，可将VB程序执行的操作完全记录下来。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/smartCHK v6.2/SMARTCHK.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/smartCHK v6.2.zip",
        	},
        	{
        	"name":"VBDebugEvents",
        	"msg":"是一款VB逆向辅助工具，可以自动查看VB按钮事件地址。",
        	"openPath":"C:/CuckooFile/packing/Tools/Disassemblers/VB-Debug Events/VBDebugEvents.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Disassemblers/VB-Debug Events.zip",
        	},

    ];
    fanhuibian.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#fanhuibianBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#fanhuibianTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: fanhuibian.initColumn(),
        data: data,
        limit:30
    });
    table.on('tool(' + fanhuibian.tableId + ')', function (obj) {

        var data = obj.data;
        var path = data.path;
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(fanhuibian.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
        if (event === 'download') {
     	   
               
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

