layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var jinchengjiance = {
        tableId: "jinchengjianceTable"
    };

    // 渲染表格
    var data = [
        {

            "name": "ProcessMonitor",
            "msg": "相当于Filemon+Regmon，其中的Filemon专门用来监视系统中的任何文件操作过程，而Regmon用来监视注册表的读写操作过程。",
            "openPath": "C:/CuckooFile/packing/ProcessMonitor_v3.82/ProcessMonitor_v3.82.exe",
            "downloadPath":"C:/CuckooFile/packing/ProcessMonitor_v3.82.zip"


        }, {

        	  "name": "procexp",
              "msg": "processexplorer是一款免费的增强型任务管理器。通过processexplorer可以看到您电脑在运行的程序和CPU、内存的使用情况，方便您管理电脑的后台程序。",
              "openPath": "C:/CuckooFile/packing/Process_Explorer_15.05/Process Explorer/procexp.exe",
              "downloadPath":"C:/CuckooFile/packing/Process_Explorer_15.05.zip"


        },{
        	"name":"PowerTool64",
        	"msg":"是一款免费的病毒查找工具，能够帮助你找出病毒木马在你的电脑中动过的手脚，并去除病毒设下的机关，为用户提供了系统修复、进程管理、内核模块、应用层、注册表、启动项等等。",
        	"openPath":"C:/CuckooFile/packing/Tools/AntiRootkit/PowerTool64.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/AntiRootkit/PowerTool64.zip",
        	},
        	{
        	"name":"apimonitor-x64",
        	"msg":"APIMonitor是一款用来监视和显示用户应用程序和服务程序中的WindowsAPI调用的免费软件。它是一个强大的工具，在跟踪调试你开发的应用程序时，可以帮助发现产生问题可能的原因。APIMonitor支持windows7及windows64位系统。",
        	"openPath":"C:/CuckooFile/packing/Tools/AntiRootkit/API Monitor/apimonitor-x64.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/AntiRootkit/API Monitor.zip",
        	},
        	{
        	"name":"sniffer",
        	"msg":"是用于网络数据抓包的工具，能够抓取网络上所有网络流动信息的数据。",
        	"openPath":"C:/CuckooFile/packing/Tools/AntiRootkit/sniffer1.02/sniffer1.02.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/AntiRootkit/sniffer1.02.zip",
        	},
        	{
        	"name":"Bginfo",
        	"msg":"是一款用于设置墙纸背景文本显示的软件，此软件本身可以探测一些有用的系统信息并能把这些有用文字显示在墙纸上，从此系统信息查看变得十分简单。",
        	"openPath":"C:/CuckooFile/packing/Tools/AntiRootkit/SysinternalsSuite/Bginfo.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/AntiRootkit/SysinternalsSuite.zip",
        	},
        	{
        	"name":"SysinternalsSuite Process Exploer",
        	"msg":"SysinternalsSuite是微软发布的一套免费工具程序集，一共包括74个windows工具。ProcessExplorer是进程浏览器，查看进程的详细信息包括CPU，GPU，IO，线程，句柄，内存。",
        	"openPath":"C:/CuckooFile/packing/Tools/AntiRootkit/SysinternalsSuite/procexp.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/AntiRootkit/SysinternalsSuite.zip",
        	},
        	{
        	"name":"vmmap",
        	"msg":"是一款专门用于分析应用程序使用虚拟和物理内存情况的软件，软件可以优化您的应用程序的内存使用量。采用图形化的显示方法，可显示摘要信息和详细进程的内存映射，VMMap支持多种形式。",
        	"openPath":"C:/CuckooFile/packing/Tools/AntiRootkit/SysinternalsSuite/vmmap.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/AntiRootkit/SysinternalsSuite.zip",
        	},

    ];
    jinchengjiance.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#jinchengjianceBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#jinchengjianceTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: jinchengjiance.initColumn(),
        data: data
    });
    table.on('tool(' + jinchengjiance.tableId + ')', function (obj) {

        var data = obj.data;
        var path = data.path;
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(jinchengjiance.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
        if (event === 'download') {
     	   
               
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

