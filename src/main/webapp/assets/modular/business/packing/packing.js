layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var packing = {
        tableId: "packingTable"
    };

    // 渲染表格
    var data = [
        {

            "name": "PEiD",
            "msg": "PEiD简介内容",
            "openPath": "C:/CuckooFile/packing/HA.PEiD.0.95/PEiD.exe",
            "downloadPath":"C:/CuckooFile/packing/HAPEiD_jb51.rar"


        }, {

        	  "name": "PEiD",
              "msg": "PEiD简介内容",
              "openPath": "C:/CuckooFile/packing/HA.PEiD.0.95/PEiD.exe",
              "downloadPath":"C:/CuckooFile/packing/HAPEiD_jb51.rar"


        }, {

        	  "name": "PEiD",
              "msg": "PEiD简介内容",
              "openPath": "C:/CuckooFile/packing/HA.PEiD.0.95/PEiD.exe",
              "downloadPath":"C:/CuckooFile/packing/HAPEiD_jb51.rar"


        }
    ];
    packing.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#packingBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#packingTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: packing.initColumn(),
        data: data
    });
    table.on('tool(' + packing.tableId + ')', function (obj) {

        var data = obj.data;
        var path = data.path;
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(packing.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
       if (event === 'download') {
     	   
               
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

