layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var tuoke = {
        tableId: "tuokeTable"
    };

    // 渲染表格
    var data = [
        {

            "name": "UPXShell",
            "msg": "UPXShell是一款应用程序专用压缩、解压缩软件，支持EXE、COM、DLL、SYS、OCX等多种文件格式的压缩。",
            "openPath": "C:/CuckooFile/packing/UPXShell/UPXShell.exe",
            "downloadPath":"C:/CuckooFile/packing/UPXShell.zip"


        }, {	
        	"name":"AoRE_Unpacker",
        	"msg":"一款通用脱壳工具。",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/AoRE_Unpacker_0.4/AoRE_Unpacker.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/AoRE_Unpacker_0.4.zip",
        	},	
        	{	
        	"name":"ArmaGeddon",
        	"msg":"ArmaGeddon(穿山甲通用脱壳软件)是一款十分优秀实用的通用脱壳辅助工具。软件提供类似C的表达式解析器，全功能的DLL和exe文件调试，类似IDA的侧边栏和跳转箭头，动态识别模块和字符串，快速反汇编以及许多有用的功能，如作为调试脚本语言自动化。",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/ArmaGeddon/ArmaGeddon.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/ArmaGeddon.zip",
        	},	
        	{	
        	"name":"ArmInline",
        	"msg":"ArmInline是一个很不错的Armadillo脱壳辅助工具，支持Armadillov3.5-4.4。",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/ArmInline0.96 最终版/ArmInline.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/ArmInline0.96 最终版.zip",
        	},	
        	{	
        	"name":"DecomAS",
        	"msg":"一款专门用于对ASpacker加壳的程序进行脱壳操作",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/ASProtect_unpaker_by_PE_Kill_完整汉化版/DecomAS.hh.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/ASProtect_unpaker_by_PE_Kill_完整汉化版.zip",
        	},	
        	{	
        	"name":"CoolDumpper",
        	"msg":"一款通用的脱壳工具，提供简洁化的脱壳操作",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/cooldumpper/loader.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/cooldumpper.zip",
        	},	
        	{	
        	"name":"DDeM Protector",
        	"msg":"软件破解专用工具中的一种脱壳工具",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/DDeM Protector 脱壳机/UnDDeM.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/DDeM Protector 脱壳机.zip",
        	},	
        		
        	{	
        	"name":"GUnPacker",
        	"msg":"GUnpacker是一款通用的脱壳工具，采用神经网络的智能未知壳识别技术，能支持多种应用程序脱壳，具有脱壳成功率高和操作简单的特点。",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/GUnPacker0.5/GUnPacker.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/GUnPacker0.5.zip",
        	},	
        	{	
        	"name":"NETUnpack",
        	"msg":"脱壳工具NETUnpack，可以脱去DotNetReactor等加的壳",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/NETUnpack/NETUnpack.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/NETUnpack.zip",
        	},	
        	{	
        	"name":"RlpackUnpacker",
        	"msg":"支持RLPACK1.20~1.21全保护脱壳，支持Codereplace,tls,vm修复",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/Rlpack1.2x_Unpacker_V0.2/RlpackUnpacker.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/Rlpack1.2x_Unpacker_V0.2.zip",
        	},	
        	{	
        	"name":"tmdunpacker",
        	"msg":"用于TMD脱壳，自动生成脚本",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/Themida系列脱壳机/tmdunpacker/tmdunpacker.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/Themida系列脱壳机/tmdunpacker.zip",
        	},	
        	{	
        	"name":"UnFSG2.0",
        	"msg":"一个静态脱壳工具。FSG2.0是一个手工较难脱除的壳，网上的很多脱壳工具都需要动态脱壳，但本款工具可以静态脱壳。用法：将要脱壳的文件拖进命令行窗口回车即可。",
        	"openPath":"C:/CuckooFile/packing/Tools/Unpackers/UnFSG2.0/UnFSG2.0.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/Unpackers/UnFSG2.0.zip",
        	},	
        
	

    ];
    tuoke.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#tuokeBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#tuokeTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: tuoke.initColumn(),
        data: data,
        limit: 30
    });
    table.on('tool(' + tuoke.tableId + ')', function (obj) {

        var data = obj.data;
        var path = data.path;
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(tuoke.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
        if (event === 'download') {
     	   
               
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

