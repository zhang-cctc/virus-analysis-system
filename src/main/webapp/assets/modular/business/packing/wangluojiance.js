layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var wangluojiance = {
        tableId: "wangluojianceTable"
    };

    // 渲染表格
    var data = [
        {

            "name": "Wireshark",
            "msg": "Wireshark（前称Ethereal）是一个网络封包分析软件。网络封包分析软件的功能是撷取网络封包，并尽可能显示出最为详细的网络封包资料。Wireshark使用WinPCAP作为接口，直接与网卡进行数据报文交换。",
            "openPath": "C:/CuckooFile/packing/wireshark/Wireshark.exe",
            "downloadPath":"C:/CuckooFile/packing/wireshark.zip"


        },{
        	"name":"Fiddler2",
        	"msg":"是一款数据包抓取软件，它通过代理的方式获取程序http通讯的数据，可以用其检测网页和服务器的交互情况，能够记录所有客户端和服务器间的http请求，支持监视、设置断点、甚至修改输入输出数据等功能。",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/Fiddler2/Fiddler.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/Fiddler2.zip",
        	},
        	{
        	"name":"HTTPDebuggerUI",
        	"msg":"HTTPDebuggerPro主要是用来测试调试复杂的网站应用程序，通过这款软件可以帮助用户实时跟踪显示浏览器和网站服务器之间的所有通讯信息，既是一个专业的网络抓包工具，也是一款网站开发调试工具，网站开发人员可以通过它来分析程序和互联网之间的通信，可以查看和分析任何应用程序和web服务器之间通过HTTP和HTTPS协议的通信，开发人员根据这些信息可以对网站进行优化和调试，网站运行更加稳定。",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/HTTP Debugger/HTTPDebuggerUI.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/HTTP Debugger.zip",
        	},
        	{
        	"name":"HttpSpy",
        	"msg":"用于跟踪网站的请求，实时记录请求访问，分析，可作带宽和流量分析，需要.net2.0的支持",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/HttpSpy/HttpSpy.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/HttpSpy.zip",
        	},
        	{
        	"name":"MiniSniffer",
        	"msg":"是一款网络抓包工具，拥有强大的过滤器，您可以选择不同的数据进行过滤，通过自定义抓包的方式进行相关的内容截取，软件支持TCP、UDP、ICMP、主机IP、主机端口号等内容设置，用户可以输入指定的IP或端口进行过滤。",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/MiniSniffer/MiniSniffer.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/MiniSniffer.zip",
        	},
        	{
        	"name":"PackAssist",
        	"msg":"是一个能拦截网络应用程序数据包的纯绿色软件，包括Send、Recv、WSASend、WSARecv、SendTo、RecvFrom、WSASendTo、WSARecvFrom。封包助手还可以拦截Connect和Accept函数，使您能够知道您的网络程序何去何从。",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/PackAssist/PackAssist.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/PackAssist.zip",
        	},
        	{
        	"name":"smsniff",
        	"msg":"是一款网络数据包捕获软件，它可以即时捕获从网卡中出入的TCP/IP数据，支持ICP/UDP/ICMP等多网络通信协议，可以用16进制或ASCII方式来显示数据，捕获的数据可以以HTML格式导出。",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/Smsniff-x64/smsniff.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/Smsniff-x64.zip",
        	},
        	{
        	"name":"WiresharkPortable",
        	"msg":"WiresharkPortable(网络协议分析器)是一款unix和windows上的开源网络协议分析器。它可以实时检测网络通讯数据，也可以检测其抓取的网络通讯数据快照文件。可以通过图形界面浏览这些数据，可以查看网络通讯数据包中每一层的详细内容。",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/WiresharkPortable/WiresharkPortable.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/WiresharkPortable.zip",
        	},
        	{
        	"name":"WSockExpert",
        	"msg":"是一款http抓包工具，用于监视和修改网络发送和接收数据的程序的软件，可以用来帮助您调试网络应用程序，分析网络程序的通信协议，支持修改发送的数据。",
        	"openPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/WSockExpert/WSockExpert_Cn.exe",
        	"downloadPath":"C:/CuckooFile/packing/Tools/NetworkAnalyzer/WSockExpert.zip",
        	},

    ];
    wangluojiance.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#wangluojianceBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#wangluojianceTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: wangluojiance.initColumn(),
        data: data
    });
    table.on('tool(' + wangluojiance.tableId + ')', function (obj) {

        var data = obj.data;
        var path = data.path;
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(wangluojiance.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
        if (event === 'download') {
     	   
               
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

