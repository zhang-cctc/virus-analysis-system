layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var zhucebiaojiance = {
        tableId: "zhucebiaojianceTable"
    };

    // 渲染表格
    var data = [
/*{
	"name":"cryptocal",
	"msg":"",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/christal_cryptotool12/cryptocal.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/christal_cryptotool12.zip",
	},*/
	{
	"name":"HashCalc",
	"msg":"较为通用的HASH计算器，可以计算任意文件的HASH值，包括CRC、MD5、SHA256等",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/DAMN_Hash_Calculator/DAMN_HashCalc.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/DAMN_Hash_Calculator.zip",
	},
	{
	"name":"DSAToolv13",
	"msg":"DSS/DSA密钥对生成工具，通过采集软件界面中的焦点实现随机生成的功能",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/DSAtoolv13/DSAToolv13.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/DSAtoolv13.zip",
	},
	{
	"name":"ecctool",
	"msg":"根据设定参数和CPU频率及线程数随机产生RNG SALT，按照设定公式生成密钥对，同时支持校验密钥对一致性",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/ECCTOOL/ecctool.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/ECCTOOL.zip",
	},
	{
	"name":"RandomGen",
	"msg":"利用鼠标轨迹生成随机数，也用于ECC密钥对生成。",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/Elliptic Curve Builder v.1.0.0/ecb.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/Elliptic Curve Builder v.1.0.0.zip",
	},
	{
	"name":"HCD",
	"msg":"检索某一EXE文件或者BIN文件中是否含有密码算法。",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/Hash & Crypto Detector v1.4/HCD.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/Hash & Crypto Detector v1.4.zip",
	},
	{
	"name":"HashCalc",
	"msg":"一款文件HASH值计算工具，支持包括TIGER,MD2,CRC32,ADLER32,RIPEMD160等杂凑算法，同时支持自定义盐来计算HMAC",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/HashCalc/HashCalc.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/HashCalc.zip",
	},
	{
	"name":"keyAssistant",
	"msg":"密钥对生成助理，支持大数、浮点数计算，用于生成非对称密钥对",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/Keygener Assistant v2.1.0/keyAssistant.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/Keygener Assistant v2.1.0.zip",
	},
	{
	"name":"Prime Generator",
	"msg":"利用鼠标轨迹生成指定数值的随机数。",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/Prime Generator/Primegen11.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/Prime Generator.zip",
	},
	{
	"name":"PYG_TOOLS_VER5",
	"msg":"支持市面上绝大多数密码算法的加解密操作。",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/PYG密码学综合工具 v5.0.0.5/PYG_TOOLS_VER5.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/PYG密码学综合工具 v5.0.0.5.zip",
	},
	{
	"name":"RDLP",
	"msg":"用于随机产生salt，并生成RSA密钥对。",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/RDLP V1.12/RDLP.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/RDLP V1.12.zip",
	},
	{
	"name":"RSATool2v17",
	"msg":"专业的RSA密码算法综合性分析工具",
	"openPath":"C:/CuckooFile/packing/Tools/Crptography/RSA Tool 2/RSATool2v17.exe",
	"downloadPath":"C:/CuckooFile/packing/Tools/Crptography/RSA Tool 2.zip",
	},

    ];
    zhucebiaojiance.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'name', title: '名称', width: 400},
            {field: 'msg', title: '描述'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#zhucebiaojianceBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#zhucebiaojianceTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: zhucebiaojiance.initColumn(),
        data: data
    });
    table.on('tool(' + zhucebiaojiance.tableId + ')', function (obj) {

        var data = obj.data;
        var path = data.path;
        var event = obj.event;
        //打开应用
        if (event === 'open') {
        	   var httpRequest = new HttpRequest(Feng.ctxPath + "/packing/open?openPath=" + data.openPath, 'post', function (data) {
                   Feng.success("正在打开工具中!");
                   table.reload(zhucebiaojiance.tableId);
               });
               httpRequest.set(data);
               httpRequest.start(true);
            
        }
        //下载应用
        if (event === 'download') {
     	   
               
     	   	window.location.href =  Feng.ctxPath + "/packing/download?downloadPath=" + data.downloadPath;
           

        	
         
     }
        
        
    });

});

