/**
 * 规则详情对话框
 */
layui.use(['layer','form', 'admin', 'HttpRequest'], function () {
	var $ = layui.$;
    var HttpRequest = layui.HttpRequest;
    var form = layui.form;
    var admin = layui.admin;

    // 表单提交事件
    form.on('submit(btnSubmit)', function (data) {
    	 var formData = new FormData($("#ruleForm")[0]);  
		 var saveLoading = parent.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
		 $.ajax({
             url : Feng.ctxPath + '/rule/addrule',
             type : 'post',
             async: false,
             data : formData,
             cache: false,
             contentType: false,  
             processData: false, 
             success : function(data) {
             	parent.layer.close(saveLoading);
             	
             	Feng.success("添加成功!");
              //传给上个页面，刷新table用
              admin.putTempData('formOk', true);

              //关掉对话框
              admin.closeThisDialog();

             }, function (data) {
                 Feng.error("添加失败！" + data.message)
             }
	  		});
    });

});
