layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var form = layui.form;
    var func = layui.func;
    var HttpRequest = layui.HttpRequest;
    var util = layui.util;
    var upload = layui.upload;
    var layer = layui.layer;

    // 规则管理
    var rules = {
        tableId: "ruleTable"
    };

    // 初始化表格的列
    rules.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {field: 'ruleId', hide: true, title: '主键id'},
            {field: 'ruleCode', sort: true, title: '规则名称'},
            {field: 'ruleMeta', sort: true, title: '规则信息'},
            {field: 'ruleStrings', sort: true, title: '规则特征码'},
            {field: 'ruleCondition', sort: true, title: '规则条件'},
            {align: 'center', toolbar: '#ruleBar', title: '操作', width: 230}
        ]];
    };


    // 点击查询按钮
    rules.search = function () {
        var queryData = {};
        queryData['ruleCode'] = $("#ruleCode").val();
        table.reload(rules.tableId, {
            where: queryData,
            page: {curr: 1}
        });
    };


    // 点击详情
    rules.openDetails = function (data) {
        func.open({
            title: '详情',
            content: Feng.ctxPath + '/view/rule/edit?ruleId=' + data.ruleId,
            tableId: rules.tableId
        });
    };
    
    // 添加按钮点击事件
    $('#btnAdd').click(function () {
    	rules.openAddDlg();
    });
    
    // 导出按钮点击事件
    $('#btnExport').click(function () {
    	window.location.href = Feng.ctxPath + '/rule/exportrules';
    });

    
    // 下载
    rules.onFileDownload = function (data) {
       window.location.href = Feng.ctxPath + '/sysFileInfo/publicDownload?fileId=' + data.fileId;
    }


    // 点击删除
    rules.onDeleteRule = function (data) {
        var operation = function () {
            var httpRequest = new HttpRequest(Feng.ctxPath + "/rule/deleterule", 'post', function (data) {
                Feng.success("删除成功!");
                table.reload(rules.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.message + "!");
            });
            httpRequest.set(data);
            httpRequest.start(true);
        };
        Feng.confirm("是否删除?", operation);
    };




    // 渲染表格
    var tableResult = table.render({
        elem: '#' + rules.tableId,
        url: Feng.ctxPath + '/rule/rulelistpage',
        page: true,
        request: {pageName: 'pageNo', limitName: 'pageSize'}, //自定义分页参数
        height: "full-158",
        cellMinWidth: 100,
        cols: rules.initColumn(),
        parseData: Feng.parseData
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        rules.search();
    });


    // 工具条点击事件
    table.on('tool(' + rules.tableId + ')', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if (event === 'details') {
            rules.openDetails(data);
        } else if (event === 'delete') {
            rules.onDeleteRule(data);
        } 
    });

    
    /**
     * 添加规则对话框
     */
    rules.openAddDlg = function () {
        func.open({
            title: '添加规则',
            content: Feng.ctxPath + '/view/rule/add',
            tableId: rules.tableId
        });
    };
    

});
