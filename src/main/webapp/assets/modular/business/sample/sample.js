layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var form = layui.form;
    var func = layui.func;
    var HttpRequest = layui.HttpRequest;
    var util = layui.util;
    var upload = layui.upload;
    var layer = layui.layer;

    var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/getCuckooUrl",'get');
    var result = httpRequest.start();
    var cuckooUrl = result.data;
    // 样本管理
    var Sample = {
        tableId: "sampleTable"
    };

    // 初始化表格的列
    Sample.initColumn = function () {
        return [[
            
            
            {field: 'sampleId', hide: true, title: '主键id'},
            {field: 'xuhao', width:70,sort: true, title: '序号',type:'numbers'},
            {field: 'sampleZhName', sort: true, title: '样本名称'},
            {field: 'typeName', sort: true, title: '样本类别'},
            {field: 'sampleType', sort: true, title: '病毒家族'},
           /* {field: 'samplePath', sort: true, title: '存储位置'},       */    
            {field: 'sampleSuffix', sort: true, title: '样本后缀'},
            {field: 'sampleOriginName', sort: true, title: '文件名称'},
            {field: 'sampleSizeKb', sort: true, title: '样本大小-KB'},
            {
                field: 'createTime', sort: true, title: '创建时间', templet: function (d) {
                    return util.toDateString(d.createTime,'yyyy年MM月dd日');
                }
            },
            /*    {field: 'createUserName', sort: true, title: '创建人'},*/  
            {align: 'center', toolbar: '#sampleBar', title: '操作', width: 150}
        ]];
    };


    // 点击查询按钮
    Sample.search = function () {
        var queryData = {};
        queryData['sampleZhName'] = $("#sampleZhName").val();
        queryData['typeName'] = $("#typeName").val();
        //queryData['positionCode'] = $("#positionCode").val();
        table.reload(Sample.tableId, {
            where: queryData,
            page: {curr: 1}
        });
    };


    // 点击详情
    Sample.openDetails = function (data) {
        func.open({
            title: '详情',
            content: Feng.ctxPath + '/view/sampleDetails?sampleId=' + data.sampleId,
            tableId: Sample.tableId
        });
    };
    
    // 添加按钮点击事件
    $('#btnAdd').click(function () {
    	Sample.openAddDlg();
    });


    // 点击删除
    Sample.onDeleteSample = function (data) {
        var operation = function () {
            var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/deleteReally", 'post', function (data) {
                Feng.success("删除成功!");
                table.reload(Sample.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.message + "!");
            });
            httpRequest.set(data);
            httpRequest.start(true);
        };
        Feng.confirm("是否删除?", operation);
    };




    // 渲染表格
    var tableResult = table.render({
        elem: '#' + Sample.tableId,
        url: Feng.ctxPath + '/Sample/sampleListPage',
        page: true,
        request: {pageName: 'pageNo', limitName: 'pageSize'}, //自定义分页参数
        height: "full-158",
        cellMinWidth: 100,
        cols: Sample.initColumn(),
        parseData: Feng.parseData
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Sample.search();
    });


    // 工具条点击事件
    table.on('tool(' + Sample.tableId + ')', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if (event === 'sampleDetails') {
            Sample.openDetails(data);
        } else if (event === 'delete') {
            Sample.onDeleteSample(data);
        } else if (event === 'viewReport') {
        	Sample.viewReport(data);
		} else if (event === "downloadReport") {
			Sample.downloadReport(data);
		}else if(event === "show"){
			Sample.openShow(data);
		}
    });
//    弹出具体信息页面
    Sample.openShow = function (data) {
        func.open({
            title: '详细信息',
            content: Feng.ctxPath + '/view/sample/viewreport?sampleId=' + data.sampleId,
            tableId: Sample.tableId
        });
    };
    
    /**
     * 添加样本对话框
     */
    Sample.openAddDlg = function () {
        func.open({
            title: '添加样本',
            content: Feng.ctxPath + '/view/sample/add',
            tableId: Sample.tableId
        });
    };
    
    /**
     * 查看报告
     */
    Sample.viewReport = function (data) {
//    	func.open({
//    		title: '查看报告',
//    		content: Feng.ctxPath + '/view/sample/viewreport?sampleId=' + data.sampleId,
//    		tableId: Sample.tableId
//    	});
    	if (data.taskId === '' || data.taskId == null) {
  		  Feng.error("查看失败!没有任务ID请重新创建此样本!");
		}else {
			window.open(cuckooUrl  +'/tasks/report/'+ data.taskId+'/html');
		}
    };
    
    /**
     * 下载报告
     */
    Sample.downloadReport = function (data) {
    	if (data.taskId === '' || data.taskId == null) {
    		  Feng.error("下载失败!没有任务ID请重新创建此样本!");
		}else {
			var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/viewreport?sampleId=" + data.sampleId,'get');
		    var result = httpRequest.start();
		    if(result == ""){
		    	Feng.error("操作失败！任务尚未分析完成。" )
		    	return ;
		    }
		    if(!result.success){
		    	Feng.error("操作失败！" +result.message)
		    	return ;
		    }
			window.location.href = cuckooUrl  +'/tasks/report/'+ data.taskId+'/all';
//			var curWwwPath = window.document.location.href;
//	        var pathName = window.document.location.pathname;
//	        var pos = curWwwPath.indexOf(pathName);
//	        var localhostPath = curWwwPath.substring(0, pos);
//	        var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
//	        var url = localhostPath + projectName + "/assets/common/images/bg-login.jpg";
//	        window.open(url);
	
//			window.location.href = 'http://192.168.8.108:8080/assets/common/images/bg-login.jpg';
		}
    };

    
    //批量添加
    var update = upload.render({
		elem: '#btnUpload'
		,url: Feng.ctxPath + '/Sample/addSampleByFiles'        //改成您自己的上传接口
		,auto: true
		,accept: 'file'
		//,multiple: true
		//,bindAction: '#hideUpload'
		
		,done: function(res){
		 //上传完毕回调
				Feng.success("更新成功!");
				table.reload(Sample.tableId, {
		            
		            page: {curr: 1}
		        });
		}
		, error: function (err) {
				//请求异常回调
				Feng.error("更新失败！" + err.message);
		}
	  });	
});
