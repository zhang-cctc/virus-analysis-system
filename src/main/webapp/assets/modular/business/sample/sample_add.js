layui.use(['layer', 'form', 'admin', 'HttpRequest', 'upload'], function () {
    var $ = layui.$;
    var form = layui.form;
    var admin = layui.admin;
    var HttpRequest = layui.HttpRequest;
	var upload = layui.upload;
		  //选完文件后不自动上传
	var update = upload.render({
		elem: '#selectFile'
		,url: Feng.ctxPath + '/Sample/update'        //改成您自己的上传接口
		,auto: false
		,accept: 'file'
		//,multiple: true
		,bindAction: '#hideUpload'
		,before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
			 this.data = {
					 sampleId: $('#sampleId').val(),
					 sampleType: $('#sampleType').val(),
					 sampleZhName: $('#sampleZhName').val(),
					 sampleEnName: $('#sampleEnName').val(),
					 sampleIntroduction: $('#sampleIntroduction').val()
			 };
		}
		,done: function(res){
		 //上传完毕回调
				Feng.success("更新成功!");
				
		}
		, error: function (err) {
				//请求异常回调
				Feng.error("更新失败！" + err.message);
		}
	  });	
	
	form.on('submit(submitadd)', function(data){
	   /* var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/testCuckooApi",'get');
	    var result = httpRequest.start();
	    if(!result.success){
	    	Feng.error("操作失败！" +result.message)
	    	return ;
	    }
		 console.log(data.field) */
		 var formData = new FormData($( "#sampleForm" )[0]);  
		 console.info(formData);
		 //parent.layer.msg("2");
		 var saveLoading = parent.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
		 $.ajax({
               url : Feng.ctxPath + '/Sample/addSample',
               type : 'post',
               async: false,
               data : formData,
               cache: false,
               contentType: false,  
               processData: false, 
               success : function(data) {
            	   parent.layer.close(saveLoading);
	        	   if(data.success){
	        		   Feng.success("添加成功!");
	                   //传给上个页面，刷新table用
	                   admin.putTempData('formOk', true);
	
	                   //关掉对话框
	                   admin.closeThisDialog();	  
	        	   }else {
	        		   Feng.error("添加失败！" + data.message)
	        	   } 
               }, function (data) {
                   Feng.error("添加失败！" + data.message)
               }
	  		});
	});
});
