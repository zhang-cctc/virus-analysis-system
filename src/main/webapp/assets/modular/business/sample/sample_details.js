layui.use(['layer', 'form', 'admin', 'HttpRequest', 'upload'], function () {
    var $ = layui.$;
    var form = layui.form;
    var HttpRequest = layui.HttpRequest;
	var upload = layui.upload;
	var admin = layui.admin;
    //获取详情信息，填充表单
    var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/detail?sampleId=" + Feng.getUrlParam("sampleId"),'get');
    var result = httpRequest.start();
    form.val("sampleForm", result.data);
 
	 // 表单提交事件
    form.on('submit(submit111)', function (data) {
    	 var formData = new FormData($("#sampleForm")[0]);  
		 var saveLoading = parent.layer.msg('数据提交中，请稍候',{icon: 16,time:false,shade:0.8});
		 $.ajax({
             url : Feng.ctxPath + '/Sample/update',
             type : 'post',
             async: false,
             data : formData,
             cache: false,
             contentType: false,  
             processData: false, 
             success : function(data) {
             	parent.layer.close(saveLoading);
             	
             	Feng.success("添加成功!");
              //传给上个页面，刷新table用
              admin.putTempData('formOk', true);

              //关掉对话框
              admin.closeThisDialog();

             }, function (data) {
                 Feng.error("添加失败！" + data.message)
             }
	  		});
    });

	
});
