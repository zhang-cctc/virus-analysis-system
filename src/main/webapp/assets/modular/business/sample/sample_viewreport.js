layui.use(['layer', 'form', 'admin', 'HttpRequest'], function () {
    var $ = layui.$;
    var form = layui.form;
    var HttpRequest = layui.HttpRequest;
	var upload = layui.upload;
    //获取详情信息，填充表单
    var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/viewreport?sampleId=" + Feng.getUrlParam("sampleId"),'get');
    var result = httpRequest.start();
    console.log(result.data);
    if(result == ""){
    	Feng.error("操作失败！任务尚未分析完成。" )
    	return ;
    }
    if(!result.success){
    	Feng.error("操作失败！" +result.message)
    	return ;
    }
    var data =JSON.parse(result.data);
    var datademo={
    	"target": {
            "category": "file", 
            "file": {
                "yara": [], 
                "sha1": "61e028af9a6bf9b305533fc436e38120167ae58c", 
                "name": "LdapAdmin.exe", 
                "type": "PE32 executable (GUI) Intel 80386, for MS Windows", 
                "sha256": "91d2bce47e93c0609b7f5727369cf4ed8ddc3b1d15f7f5bb2b2cd53770005ef5", 
                "urls": [
                    "http://www.dsml.org/DSML", 
                    "http://www.ldapadmin.org", 
                    "http://www.ldapadmin.org/download/templates"
                ], 
                "crc32": "C74C0361", 
                "path": "C:\\Users\\cctc\\.cuckoo\\storage\\analyses\\30\\binary", 
                "ssdeep": null, 
                "size": 1790464, 
                "sha512": "d59d0e02226fe8606bb98a739ed3ceb0243fb28891e25be396cbb866ccf2e117f7d12a4ca961d60acecd8244513555bf86442f832a6832273b4ff3896bb1f4cc", 
                "md5": "2526a43ea10a7b6248aa10b22fa48aa6"
            }
    	},
    	"debug": {
            "action": [
                "gatherer"
            ], 
            "dbgview": [], 
            "errors": [
                "Unable to stop auxiliary module: Sniffer\nTraceback (most recent call last):\n  File \"c:\\users\\cctc\\pycharmprojects\\pythonproject\\venv\\lib\\site-packages\\cuckoo\\core\\plugins.py\", line 164, in stop\n    module.stop()\n  File \"c:\\users\\cctc\\pycharmprojects\\pythonproject\\venv\\lib\\site-packages\\cuckoo\\auxiliary\\sniffer.py\", line 156, in stop\n    (out, err, faq(\"permission-denied-for-tcpdump\"))\nCuckooOperationalError: Error running tcpdump to sniff the network traffic during the analysis; stdout = '' and stderr = '\\n********************************************************************\\n**                                                                **\\n**              Tcpdump v4.9.2 (September 03, 2017)               **\\n**                   http://www.tcpdump.org                       **\\n**                                                                **\\n** Tcpdump for Windows is built with Microolap Packet Sniffer SDK **\\n**              Microolap EtherSensor product family              **\\n**               >>> build 5072.01 June 10, 2019 <<<              **\\n**                                                                **\\n**        Copyright(c) 1997 - 2019 Microolap Technologies         **\\n**       http://microolap.com/products/network/ethersensor        **\\n**         http://microolap.com/products/network/tcpdump          **\\n**                                                                **\\n**                  XP/2003/Vista/2008/Win7/Win8                  **\\n**                 Win2012/Win10/Win2016/Win2019                  **\\n**               (UEFI and Secure Boot compatible)                **\\n**                                                                **\\n**                       Trial license.                           **\\n**                                                                **\\n********************************************************************\\n\\nPacket Sniffer SDK (PSSDK) driver is not loaded, please run tcpdump.exe at least once with administrator rights. After that, tcpdump.exe will work correctly under accounts with any rights.\\r\\n'. Did you enable the extra capabilities to allow running tcpdump as non-root user and disable AppArmor properly (the latter only applies to Ubuntu-based distributions with AppArmor, see also https://cuckoo.sh/docs/faq/index.html#permission-denied-for-tcpdump)?"
            ], 
            "log": [
                "2021-05-11 19:11:00,000 [analyzer] DEBUG: Starting analyzer from: C:\\tmpaxtpuk\r\n", 
                "2021-05-11 19:11:00,015 [analyzer] DEBUG: Pipe server name: \\??\\PIPE\\sNBqKLqRIbfgGTccXJYJs\r\n", 
                "2021-05-11 19:11:00,015 [analyzer] DEBUG: Log pipe server name: \\??\\PIPE\\aBkoKUQXiwKMcBmzFDqTMqLWcbSdx\r\n", 
                "2021-05-11 19:11:00,015 [analyzer] DEBUG: No analysis package specified, trying to detect it automagically.\r\n", 
                "2021-05-11 19:11:00,015 [analyzer] INFO: Automatically selected analysis package \"exe\"\r\n", 
                "2021-05-11 19:11:00,092 [analyzer] DEBUG: Started auxiliary module DbgView\r\n", 
                "2021-05-11 19:11:00,529 [analyzer] DEBUG: Started auxiliary module Disguise\r\n", 
                "2021-05-11 19:11:00,686 [analyzer] DEBUG: Loaded monitor into process with pid 496\r\n", 
                "2021-05-11 19:11:00,686 [analyzer] DEBUG: Started auxiliary module DumpTLSMasterSecrets\r\n", 
                "2021-05-11 19:11:00,686 [analyzer] DEBUG: Started auxiliary module Human\r\n", 
                "2021-05-11 19:11:00,701 [analyzer] DEBUG: Started auxiliary module InstallCertificate\r\n", 
                "2021-05-11 19:11:00,701 [analyzer] DEBUG: Started auxiliary module Reboot\r\n", 
                "2"
            ], 
        }, 
    };
    
    document.getElementById("targetName").innerHTML=data.target.file.name;
    document.getElementById("size").innerHTML=data.target.file.size;
    document.getElementById("type").innerHTML=data.target.file.type;
    document.getElementById("md5").innerHTML=data.target.file.md5;
    document.getElementById("sha1").innerHTML=data.target.file.sha1;
    document.getElementById("sha256").innerHTML=data.target.file.sha256;
//    document.getElementById("sha512").innerHTML=data.target.file.sha512;
    document.getElementById("crc32").innerHTML=data.target.file.crc32;
//    document.getElementById("ssdeep").innerHTML=data.target.file.ssdeep;
//    document.getElementById("yara").innerHTML=data.target.file.yara;
    var logs=[];
    if(data.debug.log.length==0){
    	logs.push("暂无日志信息");
    }else{
    	logs=data.debug.log;
    }
    for(var i in logs){
    	var element = 	document.createElement("p");
    	
    	var b=element.nodeName;
    	var v=element.nodeType;
    	
    	document.querySelector(".logInfo").appendChild(element);     //添加到body 子节点下
    	element.innerHTML=logs[i]; 
    	
    	
    }
   // document.querySelector(".logInfo").innerHTML=logs;
    console.log(data.debug.log);
    
//    document.getElementById("img_sample_view").src = Feng.ctxPath+"/assets/modular/business/blackmailVirus/111.png";
    /*var SampleView = {
        tableId: "sampleViewTable"
    };*/
 
});
