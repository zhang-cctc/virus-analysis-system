layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var form = layui.form;
    var func = layui.func;
    var HttpRequest = layui.HttpRequest;
    var util = layui.util;
    var upload = layui.upload;
    var layer = layui.layer;

    // 规则管理
    var types = {
        tableId: "typeTable"
    };

    // 初始化表格的列
    types.initColumn = function () {
        return [[
                 {field: 'xuhao', width:70,sort: true, title: '序号',type:'numbers'},
            {field: 'typeId', hide: true, title: '主键id'},
            {field: 'typeName', sort: true, title: '类别名称'},
            {field: 'typeEnglish', sort: true, title: '英文名称'},
            {field: 'typeNote', sort: true, title: '简介'},
            {
                field: 'createTime', sort: true, title: '创建时间', templet: function (d) {
                    return util.toDateString(d.createTime,'yyyy年MM月dd日');
                }
            },
            {align: 'center', toolbar: '#typeBar', title: '操作', width: 150}
        ]];
    };


    // 点击查询按钮
    types.search = function () {
        var queryData = {};
        queryData['typeName'] = $("#typeName").val();
        queryData['typeEnglish'] = $("#typeEnglish").val();
        table.reload(types.tableId, {
            where: queryData,
            page: {curr: 1}
        });
    };


    // 点击修改
    types.openDetails = function (data) {
        func.open({
            title: '修改',
            content: Feng.ctxPath + '/view/sampleNew/type/edit?typeId=' + data.typeId,
            tableId: types.tableId
        });
    };
    
    // 添加按钮点击事件
    $('#btnAdd').click(function () {
    	types.openAddDlg();
    });
    

    // 点击删除
    types.onDeletetype = function (data) {
        var operation = function () {
            var httpRequest = new HttpRequest(Feng.ctxPath + "/type/delete", 'post', function (data) {
                Feng.success("删除成功!");
                table.reload(types.tableId);
            }, function (data) {
                Feng.error("删除失败!" + data.message + "!");
            });
            httpRequest.set(data);
            httpRequest.start(true);
        };
        Feng.confirm("是否删除?", operation);
    };




    // 渲染表格
    var tableResult = table.render({
        elem: '#' + types.tableId,
        url: Feng.ctxPath + '/type/listpage',
        page: true,
        request: {pageName: 'pageNo', limitName: 'pageSize'}, //自定义分页参数
        height: "full-158",
        cellMinWidth: 10,
        cols: types.initColumn(),
        parseData: Feng.parseData
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        types.search();
    });


    // 工具条点击事件
    table.on('tool(' + types.tableId + ')', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if (event === 'detail') {
            types.openDetails(data);
        } else if (event === 'delete') {
            types.onDeletetype(data);
        } 
    });

    
    /**
     * 添加规则对话框
     */
    types.openAddDlg = function () {
        func.open({
            title: '添加规则',
            content: Feng.ctxPath + '/view/sampleNew/type/add',
            tableId: types.tableId
        });
    };
    

});
