layui.use(['layer', 'element','util','HttpRequest'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var element = layui.element;
        var HttpRequest = layui.HttpRequest;
        var util = layui.util;
        // 查询管理
        var SampleStatic = {};
        SampleStatic.loadStatic = function(url,method) {
        	var httpRequest = new HttpRequest(Feng.ctxPath + url,method);
		    var result = httpRequest.start();
		    if(result == ""){
		    	Feng.error("统计失败" )
		    	return '';
		    }
		    if(!result.success){
		    	Feng.error("统计失败！" +result.message)
		    	return '';
		    }
		    return result.data;
        }

   
     // 各类型文件排行
        var barData = SampleStatic.loadStatic("/Sample/staticTypeTop","get");
        var barXData = barData.xData;
        var barYData = barData.yData;
        if(barXData == ''){
        	barXData = [];
        }
        if(barYData == ''){
        	barYData = [];
        }
        var sampleTypeBar = echarts.init(document.getElementById('sampleTypeBar'), myEchartsTheme);
        var option3 = {
            title: {
                text: '',
                textStyle: {
                    color: '#000',
                    fontSize: 10,
                }
            },
            tooltip: {},
            grid: {
            	top:'-10',
                left: '0',
                right: '0',
                bottom: '0',
                containLabel: true
            },
            
            xAxis: {
            	type: 'category',
                axisLabel: { interval: 0, rotate: 30 },
                data: barXData
            },
            dataZoom: [
                       {
                           type: 'inside'
                       }
                   ],
            axisLabel: {
                interval:0,
                rotate:40
             },
            yAxis: {
            	type: 'value'
            },
            series: [{
            	name:"数量",
                type: 'bar',
                data: barYData,
               // barMaxWidth: 45
            }]
        };
        sampleTypeBar.setOption(option3);


     // Enable data zoom when user click bar.
     var zoomSize = 6;
     sampleTypeBar.on('click', function (params) {
    	 console.log(barXData[Math.max(params.dataIndex - zoomSize / 2, 0)]);
         sampleTypeBar.dispatchAction({
             type: 'dataZoom',
             startValue: barXData[Math.max(params.dataIndex - zoomSize / 2, 0)],
             endValue: barXData[Math.min(params.dataIndex + zoomSize / 2, barYData.length - 1)]
         });
     });
        // 窗口大小改变事件
        window.onresize = function () {

        	sampleTypeBar.resize();
        };

    });