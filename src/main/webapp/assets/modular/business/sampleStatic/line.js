layui.use(['layer', 'element','util','HttpRequest'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var element = layui.element;
        var HttpRequest = layui.HttpRequest;
        var util = layui.util;
        // 查询管理
        var SampleStatic = {};
        SampleStatic.loadStatic = function(url,method) {
        	var httpRequest = new HttpRequest(Feng.ctxPath + url,method);
		    var result = httpRequest.start();
		    if(result == ""){
		    	Feng.error("统计失败" )
		    	return '';
		    }
		    if(!result.success){
		    	Feng.error("统计失败！" +result.message)
		    	return '';
		    }
		    return result.data;
        }

   
        // 每日上传文件数量统计
        var sampleFileDayLine = echarts.init(document.getElementById('sampleFileDayLine'), myEchartsTheme);
        var lineData = SampleStatic.loadStatic("/Sample/staticFileCount","get");
        var lineXData = lineData.xData;
        var lineYData = lineData.yData;
        if(lineXData == ''){
        	lineXData = [];
        }
        if(lineYData == ''){
        	lineYData = [];
        }
        var option = {
            title: {
                text: '',
                subtext: '',
                textStyle: {
                    color: '#000'
                }
            },
            tooltip: {
                trigger: "axis"
            },
            grid: {
            	top: '-3',
                left: '25',
                right: '0',
                bottom: '0',
                containLabel: true
            },
            xAxis: [{
                type: "category",
                boundaryGap: !1,
                axisLabel:{
            		interval:0,
                	rotate: 20
                },
                data: lineXData
            }],
            yAxis: [{
                type: "value"
            }],
            series: [{
                name: "数量",
                type: "line",
                smooth: !0,
                itemStyle: {
                    normal: {
                        areaStyle: {
                            type: "default"
                        }
                    }
                },
                data: lineYData
            }]
        };
        sampleFileDayLine.setOption(option);

 
        // 窗口大小改变事件
        window.onresize = function () {

        	sampleFileDayLine.resize();
        };

    });