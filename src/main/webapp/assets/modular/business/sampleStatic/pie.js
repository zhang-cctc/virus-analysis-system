layui.use(['layer', 'element','util','HttpRequest'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var element = layui.element;
        var HttpRequest = layui.HttpRequest;
        var util = layui.util;
        // 查询管理
        var SampleStatic = {};
        SampleStatic.loadStatic = function(url,method) {
        	var httpRequest = new HttpRequest(Feng.ctxPath + url,method);
		    var result = httpRequest.start();
		    if(result == ""){
		    	Feng.error("统计失败" )
		    	return '';
		    }
		    if(!result.success){
		    	Feng.error("统计失败！" +result.message)
		    	return '';
		    }
		    return result.data;
        }

   
        // 各类型文件排行
        var pieData = SampleStatic.loadStatic("/Sample/staticTypePer","get");
        if(pieData == ''){
        	pieData = [];
        }
        var sampleTypePie = echarts.init(document.getElementById('sampleTypePie'), myEchartsTheme);
        optionPie = {
        	    title: {
        	        text: '',
        	        subtext: '',
        	        left: 'center'
        	    },
        	    tooltip: {
        	        trigger: 'item',
        	       
        	    },
        	    legend: {
        	        orient: 'vertical',
        	        left: 'left',
        	    },
        	    series: [
        	        {
        	            name: '样本类型',
        	            type: 'pie',
        	            radius: '80%',
        	            data: pieData,
        	        
        	            itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            },
                            normal:{ 
                                label:{ 
                                    show: true, 
                                  //  formatter: '{b} : {c} ({d}%)' 
                                    formatter: '{b} : {d}%'
                                }, 
                                labelLine :{show:true} 
                            } 
                        }
        	        }
        	    ]
        	};
        sampleTypePie.setOption(optionPie);
        
 
        // 窗口大小改变事件
        window.onresize = function () {

        	sampleTypePie.resize();
        };

    });