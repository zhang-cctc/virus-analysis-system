layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var HttpRequest = layui.HttpRequest;
    // 脱壳
    var tools = {
        tableId: "toolsTable"
    };

    // 渲染表格
    var data = [
        {
            "title": "2020年勒索病毒疫情分析报告",
            "source": "360安全中心",
            "remark": "针对勒索病毒疫情的分析报告",
            "createTime": "2021-06-02",
            "path": "C:/CuckooFile/virus/reports/2020年勒索病毒疫情分析报告.pdf"
        },
        {
        	 "title": "Satan分析",
             "source": "360安全中心",
             "remark": "针对Satan勒索病毒的分析报告",
             "createTime": "2021-06-02",
             "path": "C:/CuckooFile/virus/reports/Satan分析.docx"
        },
        {
        	 "title": "Sodinokibi分析报告",
             "source": "360安全中心",
             "remark": "针对Sodinokibi勒索病毒分析报告",
             "createTime": "2021-06-02",
             "path": "C:/CuckooFile/virus/reports/Sodinokibi分析.docx"
        },
        {
       	 "title": "STOP分析",
            "source": "360安全中心",
            "remark": "针对STOP勒索病毒分析报告",
            "createTime": "2021-06-02",
            "path": "C:/CuckooFile/virus/reports/STOP分析.docx"
       },
       {
      	 "title": "phobos分析",
           "source": "360安全中心",
           "remark": "针对phobos勒索病毒分析报告",
           "createTime": "2021-06-02",
           "path": "C:/CuckooFile/virus/reports/phobos分析.docx"
      }

    ];
    tools.initColumn = function () {
        return [[
            {type: 'numbers', title: '序号', width: 50},

            {field: 'title', title: '报告标题', width: 400},
            {field: 'source', title: '出处'},
            {field: 'remark', title: '备注'},
            {field: 'createTime', title: '创建时间'},
            {field: 'path', title: 'path', hide: true},
            {align: 'center', toolbar: '#toolsBar', title: '操作', width: 200}
        ]];
    };
    var tableResult = table.render({
        elem: '#toolsTable',
        height: "full-158",
        cellMinWidth: 100,
        cols: tools.initColumn(),
        data: data
    });
    table.on('tool(' + tools.tableId + ')', function (obj) {

        var data = obj.data;
        var event = obj.event;
        //标注选中样式
        if (event === 'viewReport') {
        	
           /* let  parm=encodeURI(data.path);
            $.get(Feng.ctxPath + "/report/view?reportPath=" + parm, function () {

            });*/

          	 var httpRequest = new HttpRequest(Feng.ctxPath + "/view/virus/report/view?reportPath=" + data.path, 'post', function (data) {
                 Feng.success("正在打开报告中!");
             });
             httpRequest.set(data);
             httpRequest.start(true);

        }
    });

});

