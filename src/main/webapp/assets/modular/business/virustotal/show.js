layui.use(['table', 'form', 'func', 'HttpRequest', 'util', 'upload'], function () {
    var $ = layui.$;
    var table = layui.table;
    var form = layui.form;
    var func = layui.func;
    var HttpRequest = layui.HttpRequest;
    var util = layui.util;
    var upload = layui.upload;
    var layer = layui.layer;
    // 样本管理
    var Sample = {
        tableId: "sampleTable"
    };
   
    // 初始化表格的列
    Sample.initColumn = function () {
        return [[
            
            
            {field: 'sampleId', hide: true, title: '主键id'},
            {field: 'xuhao', width:70,sort: true, title: '序号',type:'numbers'},
            {field: 'sampleZhName', sort: true, title: '样本名称'},
            {field: 'typeName', sort: true, title: '样本类别'},
            {field: 'sampleType', sort: true, title: '病毒家族'},
           /* {field: 'samplePath', sort: true, title: '存储位置'},       */    
            {field: 'sampleSuffix', sort: true, title: '样本后缀'},
            {field: 'sampleOriginName', sort: true, title: '文件名称'},
            {field: 'sampleSizeKb', sort: true, title: '样本大小-KB'},
            {
                field: 'updateTime', sort: true, title: '分析时间', templet: function (d) {
                	return util.toDateString(d.updateTime, 'yyyy年MM月dd日');
                }
            },
            /*    {field: 'createUserName', sort: true, title: '创建人'},*/  
            {align: 'center', toolbar: '#sampleBar', title: '操作', width: 150}
        ]];
    };


    // 点击查询按钮
    Sample.search = function () {
        var queryData = {};
        queryData['sampleZhName'] = $("#sampleZhName").val();
        queryData['typeName'] = $("#typeName").val();
        //queryData['positionCode'] = $("#positionCode").val();
        table.reload(Sample.tableId, {
            where: queryData,
            page: {curr: 1}
        });
    };

    
    // 添加按钮点击事件
    $('#btnAdd').click(function () {
    	Sample.openAddDlg();
    });






    // 渲染表格
    var tableResult = table.render({
    	initSort: {
            field: 'updateTime' //排序字段，对应 cols 设定的各字段名
            ,type: 'desc' //排序方式  asc: 升序、desc: 降序、null: 默认排序
          },
        elem: '#' + Sample.tableId,
        url: Feng.ctxPath + '/Sample/sampleListPageByStatus1',
        page: true,
        request: {pageName: 'pageNo', limitName: 'pageSize'}, //自定义分页参数
        height: "full-158",
        cellMinWidth: 100,
        cols: Sample.initColumn(),
        parseData: Feng.parseData
    });

    // 搜索按钮点击事件
    $('#btnSearch').click(function () {
        Sample.search();
    });


    // 工具条点击事件
    table.on('tool(' + Sample.tableId + ')', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if(event === "show"){
			Sample.viewReport(data);
			
		}
    });
    
    /**
     * 查看报告
     */
    Sample.viewReport = function (data) {
    	 	var httpRequest = new HttpRequest(Feng.ctxPath + "/Sample/viewreport?sampleId=" + data.sampleId,'get');
    	    var result = httpRequest.start();
    	    console.log(result.data);
    	    if(result == ""){
    	    	Feng.error("操作失败！任务尚未分析完成。" )
    	    	return ;
    	    }
    	    if(!result.success){
    	    	Feng.error("操作失败！" +result.message)
    	    	return ;
    	    }
    	   
    	    var data1 =JSON.parse(result.data);
    	    var sha256=data1.target.file.sha256;
			window.open("https://www.virustotal.com/gui/file/"+ sha256+"/detection");
		
    };
    
  

    
   
});
