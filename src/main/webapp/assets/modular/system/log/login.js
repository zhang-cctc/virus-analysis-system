
$(function () {

    layui.use(['layer', 'form', 'index', 'HttpRequest','admin','notice'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        var HttpRequest = layui.HttpRequest;
        var index = layui.index;
        var admin = layui.admin;
        var notice = layui.notice;

        //new SanSec().validateToken();
        // 判断是否最外层
        if(window.top != window.self){
            top.location.href = location.href;
        }

        $("#btn-login").click(function () {
            login();
            $("#btn-login").blur();
        });

        $("#btn-cancel").click(function () {
            // document.getElementById("form-login").reset();
            $('#name').val("");
            $('#pwd').val("");
        });
        $("body").keydown(function() {
            if (event.keyCode == "13") {//keyCode=13是回车键
                $('#btn-login').click();//换成按钮的id即可
            }
        });

        function login() {
            var name = $('#name').val();
            var pwd = $('#pwd').val();
            if((!pwd || pwd=='')&&(!name || name=='')){
                //layer.alert("账户名、密码不能为空", {title: '错误信息', icon: 2, time: 5000});
                layer.alert("账户名、密码不能为空", {
                    title: '错误信息', icon: 2
                });
                return;
            }else if(!pwd || pwd==''){
                //layer.alert("密码不能为空", {title: '错误信息', icon: 2, time: 5000});
                layer.alert("密码不能为空", {
                    title: '错误信息', icon: 2
                });
                return;
            }else if(!name || name==''){
                //layer.alert("账户名不能为空", {title: '错误信息', icon: 2, time: 5000});
                layer.alert("账户名不能为空", {
                    title: '错误信息', icon: 2
                });
                return;
            }
            var data= {
                "account": name,
                "password": pwd
            };

            $.ajax({
                url: Feng.ctxPath + 'login',
                type: "POST",
                contentType :"application/json",
                data:JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        window.location.href = Feng.ctxPath + "/";
                    } else {
                        alert(data.msg)
                        layer.msg(data.msg, {icon: 5, anim: 6});
                    }
                }, error: function (XMLHttpRequest) {
                    layer.msg('数据处理失败! 错误码:' + XMLHttpRequest.status + ' 错误信息:' + XMLHttpRequest.statusText, {icon: 5, anim: 6});
                }
            });
            return false;
        }




    })

});

