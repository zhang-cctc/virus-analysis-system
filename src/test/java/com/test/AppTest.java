package com.test;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.stylefeng.guns.GunsApplication;
import cn.stylefeng.guns.core.exception.BusinessException;
import cn.stylefeng.guns.modular.business.entity.Sample;
import cn.stylefeng.guns.modular.business.entity.SampleType;
import cn.stylefeng.guns.modular.business.exception.MyBusinessExceptionEnum;
import cn.stylefeng.guns.modular.business.facotry.SampleFactory;
import cn.stylefeng.guns.modular.business.service.SampleService;
import cn.stylefeng.guns.modular.business.service.TypeService;
import cn.stylefeng.guns.modular.business.utils.ZIPUtils;
import cn.stylefeng.roses.kernel.auth.api.context.LoginContext;
import cn.stylefeng.roses.kernel.auth.api.pojo.login.LoginUser;
import cn.stylefeng.roses.kernel.file.api.FileOperatorApi;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;

import static cn.stylefeng.roses.kernel.file.api.constants.FileConstants.DEFAULT_BUCKET_NAME;
import static cn.stylefeng.roses.kernel.file.api.constants.FileConstants.FILE_POSTFIX_SEPARATOR;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Decription: 
 * @author wangtao
 * @version 1.0
 * create on 2021/6/3.
 *
 */

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = GunsApplication.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AppTest {

	@Autowired
	private SampleService sampleService;
	@Autowired
	private TypeService typeService;

	//"批量上传文件 相当于addSampleByFiles
	@Test
	@Rollback(false)
	public  void uploadZip() throws Exception {
		String[] paths = {"Trojan-Downloader.zip","Trojan-Downloader (2).zip"};
		for(String path2:paths) {
			
		
		String path0 = "C:/Users/cctc/Desktop/病毒样本库/";
		String path1 = path0+path2;
		File fileTemp = new File(path1);
		String path = "temp_dir" + System.currentTimeMillis();
		File tempDir = new File(path);
		try {
			if (!tempDir.exists()) {
				System.out.println("mkdir:" + tempDir.mkdirs());
			}
			ZIPUtils.unZipFiles(fileTemp, path);

			File[] files = tempDir.listFiles();
			for (File f : files) {
				// 文件请求转换存入数据库的附件信息
				Sample saveSample = factory(f,new Sample());
				saveSample.setSampleZhName(f.getName());				
				String name = f.getName();
				//病毒家族
				String sampleType="";
				//病毒类别
				String typeName="";
				if(name.contains(".")) {
					
					String[] list = f.getName().split("\\.");
					
					if(list.length>=2) {
						//病毒家族
						sampleType = list[1];
						//病毒类别
						typeName = list[0];
					}
					
				}
				
				
				SampleType type = new SampleType();
				
				type.setTypeEnglish(typeName);
				SampleType type2 = typeService.selectBytypeName(type);
				if(null==type2 ) {
					if(!"".equals(typeName)) {
						type.setCreateTime(new Date());
						typeService.save(type);
						type2 = typeService.selectBytypeName(type);
					}
					
				}
				if(null!=type2) {
					//病毒类别
					
					saveSample.setTypeName(type2.getTypeName());
					saveSample.setTypeId(type2.getTypeId());
				}
				
				saveSample.setSampleType(sampleType);
				
				saveSample.setCreateTime(DateTimeToDate());
				// 保存附件到附件信息表
				sampleService.saveOrUpdate(saveSample);
			}

		}catch (Exception e){
			throw  e;
		}finally {
			ZIPUtils.deleteFile(tempDir);
		}
		}
	
	}
	
	public Sample factory(File file ,Sample sample) {
        sample.setSampleId(null);

        FileOperatorApi fileOperatorApi = SpringUtil.getBean(FileOperatorApi.class);

        // 生成文件的唯一id
        Long fileId = IdWorker.getId();

        // 获取文件原始名称
        String originalFilename = file.getName();

        // 获取文件后缀（不包含点）
        String fileSuffix = null;
        if (ObjectUtil.isNotEmpty(originalFilename)) {
            fileSuffix = StrUtil.subAfter(originalFilename, FILE_POSTFIX_SEPARATOR, true);
        }

        // 生成文件的最终名称
        String finalFileName = fileId + FILE_POSTFIX_SEPARATOR + fileSuffix;

        // 计算文件大小kb
        long fileSizeKb = Convert.toLong(NumberUtil.div(new BigDecimal(file.length()), BigDecimal.valueOf(1024)).setScale(0, BigDecimal.ROUND_HALF_UP));
        Date dNow = new Date( );
        



        String path =  "C:/CuckooFile" + File.separator + DEFAULT_BUCKET_NAME + File.separator + finalFileName;
        FileUtil.copy(file.getAbsolutePath(), path, true);
        // 封装存储文件信息（上传替换公共信息）
//        Sample sample = new Sample();
    
        
        sample.setSampleBucket(DEFAULT_BUCKET_NAME);
        sample.setSampleObjectName(finalFileName);
        sample.setSampleOriginName(originalFilename);
        sample.setSamplePath(path);
        sample.setSampleSuffix(fileSuffix);
        sample.setSampleSizeKb(fileSizeKb);
        sample.setSampleStatus("0");//0：未分析
 
        sample.setCreateUser(1339550467939639299L);
        sample.setCreateTime(dNow);
        sample.setCreateUserName("admin");
        // 返回结果
        return sample;
	}
	
	@Test
	public  void uploadZipTest() throws Exception{
		
		try {

			File zipRoot= new File("C://Users//cctc//Desktop//病毒样本库//test");
			File[] listFiles = zipRoot.listFiles();

			for(File f: listFiles){

				FileInputStream fileInputStream = new FileInputStream(f);
				MultipartFile multipartFile = new MockMultipartFile(f.getName(), f.getName(),
						ContentType.APPLICATION_OCTET_STREAM.toString(), fileInputStream);


				sampleService.addSampleByFile(multipartFile);
			}


		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public Date DateTimeToDate(){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String s = sdf.format(new Date());
		Date date = null;
		try {
			date = sdf.parse(s);
		} catch (ParseException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();
		}
		return date;
	}
	
	public void tes() {
		String f = "abc.sds.dgfg";
		String[] list = f.split("//.");
		if(list.length>=2) {
			//病毒家族
			System.out.println(list[1]); 
			//病毒类别
			
			System.out.println(list[0]); 
		}
	}

}
